/*
 * Tile.h
 *
 * Burak Bastem
 * January 9, 2018
 *
 */

#ifndef TILE_H_
#define TILE_H_

#include "Dimension.h"

class Tile {
public:
	// constructers
	Tile() = default; // empty constructer
	Tile(double* data, const int id, const Dimension& low, const Dimension& high, const Dimension& phys_dim, const int d_id);
	// operator overloadings
	bool operator== (const Tile& t) const { return data==t.data && id == t.id && d_id == t.d_id && low == t.low && high == t.high	&& phys_dim == t.phys_dim; };
	bool operator!= (const Tile& t) const { return !(*this == t); };
	// getters
	double* getData () const { return data; };
	int getID () const { return id; };
	const Dimension getLow () const { return low; };
	const Dimension getHigh () const { return high; };
	const Dimension getPhysDim () const { return phys_dim; };
	int d_id;

private:
	double* data; // pointer to the region to which tile belongs
	int id;
	Dimension low; // lower bounds of tile (iteration)
	Dimension high; // upper bounds of tile (iteration)
	Dimension phys_dim; // physical dimension of data that tile points, or physical dimension of region containing ghost cells
	friend class TiDA;
	friend class TileTest;
};

#endif /* TILE_H_ */
