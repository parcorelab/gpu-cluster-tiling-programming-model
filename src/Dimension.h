/*
* Dimension.h
*
* Burak Bastem
* January 9, 2018
*
*/

#ifndef DIMENSION_H_
#define DIMENSION_H_

#include <exception>

class Dimension {
public:
	// constructers
	Dimension () = default;
	Dimension (const int depth, const int height, const int width);
	// operator overloadings
	int operator() (const int dim) const { return dims[dim]; };
	bool operator== (const Dimension& d) const { return dims[0] == d.dims[0] && dims[1] == d.dims[1] && dims[2] == d.dims[2]; };
	bool operator!= (const Dimension& d) const { return !(*this == d); };
	Dimension operator+ (const Dimension& d) const { return Dimension(dims[0]+d.dims[0], dims[1]+d.dims[1], dims[2]+d.dims[2]); };
	Dimension operator- (const Dimension& d) const { return Dimension(dims[0]-d.dims[0], dims[1]-d.dims[1], dims[2]-d.dims[2]); };
	Dimension operator* (const Dimension& d) const { return Dimension(dims[0]*d.dims[0], dims[1]*d.dims[1], dims[2]*d.dims[2]); };
	Dimension operator/ (const Dimension& d) const { return Dimension(dims[0]/d.dims[0], dims[1]/d.dims[1], dims[2]/d.dims[2]); };
	Dimension operator% (const Dimension& d) const { return Dimension(dims[0]%d.dims[0], dims[1]%d.dims[1], dims[2]%d.dims[2]); };
	Dimension operator+ (const int n) const { return Dimension(dims[0]+n, dims[1]+n, dims[2]+n); };
	Dimension operator- (const int n) const { return Dimension(dims[0]-n, dims[1]-n, dims[2]-n); };
	Dimension operator* (const int n) const { return Dimension(dims[0]*n, dims[1]*n, dims[2]*n); };
	Dimension operator/ (const int n) const { return Dimension(dims[0]/n, dims[1]/n, dims[2]/n); };
	Dimension operator% (const int n) const { return Dimension(dims[0]%n, dims[1]%n, dims[2]%n); };
	const int* toArray () const;

private:
	int dims[3] = {0};
	friend class DimensionTest;
};

#endif /* DIMENSION_H_ */
