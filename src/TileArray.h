/*
* TileArray.h
*
* Burak Bastem
* January 9, 2018
*
*/

#ifndef TILEARRAY_H_
#define TILEARRAY_H_

#include "Tile.h"
#include "TiDA.h"

#include <cuda_runtime.h>

class TileArray {
public:
	TileArray (); // empty constructor
	TileArray (const Dimension& data_dim, const Dimension& region_dim, const Dimension& ghost_cell_dim, const bool use_corner_ghosts = false, const int ghost_cell_update_option = 0); // main constructor
	TileArray (const double* data, const Dimension& data_dim, const Dimension& region_dim, const Dimension& ghost_cell_dim, const bool use_corner_ghosts = false, const int ghost_cell_update_option = 0); // main constructor with given data
	TileArray (const TileArray& ta); // copy constructor
	TileArray (TileArray&& ta); // move constructor
	~TileArray (); // destructer
	TileArray& operator= (const TileArray& ta); // copy assignment operator
	TileArray& operator= (TileArray&& ta); // move assignment operator
	void printRegions (); // mainly for testing
	void updateGhostCells ();
	void updateBoundaries ();
	int getRegionCount () const { return region_count; }; // <-- will i need this, check it later
	// iterator related tile array functions
	class Iterator {
	public:
		Tile operator* () const;
		Iterator operator() (TileArray& ta) const;
		bool operator== (const Iterator& i) const;
		bool operator!= (const Iterator& i) const { return !(*this == i); };
		Iterator& operator++ ();
	private:
		TileArray* ta = nullptr;
		int region_index = 0;
		int tile_index = 0;
		Dimension tile_dim;
		const float gpu_exec_ratio = 0;
		Iterator (TileArray* ta, const int region_index, const Dimension& tile_dim, const float gpu_exec_ratio = 0);
		friend class TileArray; // gives access to constructor
		friend class TileArrayTest;
		int getTileCount () const;
	public:
		// exceptions
		class InvalidGPUExecutionRatio : public std::exception {
			virtual const char* what() const throw() {
				return "Invalid GPU Execution Ratio: GPU execution ratio given to TileArrayIterator should be between 0 and 1";
			}
		};
	};
	Iterator begin ();
	Iterator begin (const float gpu_exec_ratio);
	Iterator begin (const Dimension& tile_dim);
	Iterator end ();

private:
	int mpi_size = 0;
	int mpi_rank = 0;
	Dimension data_dim; // dimension of data
	Dimension region_dim; // dimension of regions
	Dimension ghost_cell_dim; // dimension of ghost cells
	double** regions_data = nullptr; // array of data of host regions
	double** d_regions_data = nullptr; // array of data of device regions
	bool is_data_initialized = false;
	int* device_cache = nullptr;
	cudaStream_t* device_streams = nullptr;
	#define TIDA_DEVICE_CACHE_NO_REGION -1
	#define TIDA_DEVICE_CACHE_NOT_INIT_REGION -2
	int region_count = 0;
	int device_region_count = 0;
	Dimension getRegionsLow () const { return ghost_cell_dim; };
	Dimension getRegionsHigh () const { return (ghost_cell_dim + region_dim) - 1; };
	Dimension getRegionsPhysDim () const { return region_dim + (ghost_cell_dim * 2); }; // <-- get ride of this
	int getRegionsPhysCount () const;
	double* getRegionHostPtr (const int region_index);
	double* getRegionDevicePtr (const int region_index);
	class GhostCellManager {
	public:
		GhostCellManager () = delete;
		GhostCellManager (TileArray* ta);
		GhostCellManager (const GhostCellManager& gcm) = delete; // copy constructor
		GhostCellManager (GhostCellManager&& gcm) = default; // move constructor
		~GhostCellManager (); // <-- check this
		GhostCellManager& operator= (const GhostCellManager& gcm) = delete; // copy assignment operator
		GhostCellManager& operator= (GhostCellManager&& gcm) = delete; // move assignment operator
		void init (const bool include_corners, const int ghost_cell_update_option);
		void copy (const GhostCellManager& gcm);
		void move (GhostCellManager& gcm);
		void swap (GhostCellManager& gcm);
		void allocateDeviceBuffers ();
		int getCounterGhostZone (int gz);
		void allHostUpdate (const int mpi_recv_request_count);
		void deviceInvolvedUpdate (const bool rdma_support, const int mpi_recv_request_count, const int cuda_transfer_event_count);
	private:
		TileArray* ta = nullptr;
		int ghost_zone_count = 0;
		Dimension*** ghost_zone_indices = nullptr;
		#define TIDA_SRC_LOW 0
		#define TIDA_SRC_HIGH 1
		#define TIDA_DST_LOW 2
		#define TIDA_DST_HIGH 3
		#define TIDA_COUNTER_SRC_LOW 4
		#define TIDA_COUNTER_SRC_HIGH 5
		int* zone_cell_counts = nullptr;
		int** cor_reg_indices = nullptr; // corresponding regions' indices: keeps global region indices
		double**** buffers = nullptr;
		double**** d_buffers = nullptr;
		#define TIDA_SEND_BUF 0
		#define TIDA_RECV_BUF 1
		friend class TileArray;
	};
	GhostCellManager gc_manager;
	friend class TileArrayTest;

public:
	// exceptions
	class InvalidRegionDimensionsException : public std::exception {
		virtual const char* what() const throw() {
			return "Invalid Region Dimensions: data dimensions are not divisible by region dimensions";
		}
	};
	class InvalidTileDimensionsException : public std::exception {
		virtual const char* what() const throw() {
			return "Invalid Tile Dimensions: region dimensions are not divisible by tile dimensions";
		}
	};
	class InvalidProcessCountException : public std::exception {
		virtual const char* what() const throw() {
			return "Invalid Process Count: number of regions is not divisible by number of processes";
		}
	};
	class InsufficientDeviceCountException : public std::exception {
		virtual const char* what() const throw() {
			return "Insufficient Device Count: there are more processes than devices";
		}
	};
};

#endif /* TILEARRAY_H_ */
