/*
* TileArray.cpp
*
* Burak Bastem
* January 9, 2018
*
*/

#include <iostream>
#include <cstring> // for memcpy
#include <mpi.h>
#include <openacc.h>
#include "TileArray.h"

// error handling macro (source nvidia)
#define CUDA_CHECK(call) \
if((call) != cudaSuccess) { \
  cudaError_t err = cudaGetLastError(); \
  std::cerr << "CUDA error calling \""#call"\", code is " << err << std::endl; \
  tida_abort(err); }

  void tida_abort(cudaError_t err) {} // <-- implement this

  // empty constructor
  TileArray::TileArray () :
  gc_manager(GhostCellManager(this)) {
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  }

  // main constructor
  TileArray::TileArray (const Dimension& data_dim, const Dimension& region_dim, const Dimension& ghost_cell_dim, const bool use_corner_ghosts, const int ghost_cell_update_option) :
  data_dim(data_dim),	region_dim(region_dim),	ghost_cell_dim(ghost_cell_dim),	gc_manager(GhostCellManager(this)) {
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    if (data_dim % region_dim != Dimension(0, 0, 0)) throw InvalidRegionDimensionsException();
    Dimension region_count_dim = data_dim / region_dim;
    int total_region_count = region_count_dim(0) * region_count_dim(1) * region_count_dim(2);
    region_count = total_region_count / mpi_size; // region count per process
    if (total_region_count % mpi_size != 0) throw InvalidProcessCountException();
    regions_data = new double*[region_count];
    size_t region_phys_size = getRegionsPhysCount() * sizeof(double);
    for (int i = 0; i < region_count; i++) {
      double* r;
      CUDA_CHECK(cudaMallocHost((void**)&r, region_phys_size));
      regions_data[i] = r;
    }
    gc_manager.init(use_corner_ghosts, ghost_cell_update_option); // be careful with gcm objects; they must contain a pointer to its tile array
  }

  // main constructor with given data
  TileArray::TileArray (const double* data, const Dimension& data_dim, const Dimension& region_dim, const Dimension& ghost_cell_dim, const bool use_corner_ghosts, const int ghost_cell_update_option) :
  TileArray(data_dim, region_dim, ghost_cell_dim, use_corner_ghosts, ghost_cell_update_option) {
    Dimension region_count_dim = data_dim / region_dim;
    int region_count_z = region_count_dim(0); int region_count_y = region_count_dim(1); int region_count_x = region_count_dim(2);
    int region_count_xy = region_count_x * region_count_y;
    int data_y = data_dim(1); int data_x = data_dim(2);
    Dimension regions_phys_dim = getRegionsPhysDim();
    int phys_y = regions_phys_dim(1); int phys_x = regions_phys_dim(2);
    Dimension dst_low = getRegionsLow();
    int dst_low_z = dst_low(0); int dst_low_y = dst_low(1); int dst_low_x = dst_low(2);
    Dimension dst_high = getRegionsHigh();
    int dst_high_z = dst_high(0); int dst_high_y = dst_high(1); int dst_high_x = dst_high(2);
    for (int r = 0; r < region_count; r++) {
      int region_index = mpi_rank * region_count + r;
      int region_z = region_index / region_count_xy;
      int remaining = region_index % region_count_xy; // compiler likely uses remainder of previous division (see implementation notes)
      int region_y = remaining / region_count_x;
      int region_x = remaining % region_count_x; // compiler likely uses remainder of previous division (see implementation notes)
      Dimension src_low = Dimension(region_z, region_y, region_x) * region_dim;
      int src_low_z = src_low(0); int src_low_y = src_low(1); int src_low_x = src_low(2);
      double* dst_region = regions_data[r];
      for (int i1 = dst_low_z, i2 = src_low_z; i1 <= dst_high_z; i1++, i2++) {
        for (int j1 = dst_low_y, j2 = src_low_y; j1 <= dst_high_y; j1++, j2++) {
          for (int k1 = dst_low_x, k2 = src_low_x; k1 <= dst_high_x; k1++, k2++) {
            dst_region[(i1 * phys_y + j1) * phys_x + k1] = data[(i2 * data_y + j2) * data_x + k2];
          }
        }
      }
    }
    is_data_initialized = true;
  }

  // copy constructor
  TileArray::TileArray (const TileArray& ta) :
  mpi_size(ta.mpi_size), mpi_rank(ta.mpi_rank), data_dim(ta.data_dim), region_dim(ta.region_dim), ghost_cell_dim(ta.ghost_cell_dim),
  region_count(ta.region_count), device_region_count(ta.device_region_count), gc_manager(GhostCellManager(this)) {
    // copy host regions' data
    regions_data = new double*[region_count];
    int region_phys_count = getRegionsPhysCount();
    size_t region_phys_size = region_phys_count * sizeof(double);
    for (int i = 0; i < region_count; i++) {
      double* src_r = ta.regions_data[i];
      double* r;
      CUDA_CHECK(cudaMallocHost((void**)&r, region_phys_size));
      CUDA_CHECK(cudaMemcpy(r, src_r, region_phys_size, cudaMemcpyHostToHost));
      regions_data[i] = r;
    }
    // copy device components
    if (device_region_count != 0) {
      d_regions_data = new double*[device_region_count];
      for (int i = 0; i < device_region_count; i++) {
        double* src_r = ta.d_regions_data[i];
        double* r;
        CUDA_CHECK(cudaMalloc((void **)&r, region_phys_size));
        CUDA_CHECK(cudaMemcpy(r, src_r, region_phys_size, cudaMemcpyDeviceToDevice));
        d_regions_data[i] = r;
      }
      device_cache = new int[device_region_count];
      CUDA_CHECK(cudaMemcpy(device_cache, ta.device_cache, device_region_count*sizeof(int), cudaMemcpyHostToHost));
    }
    // copy ghost cell manager
    gc_manager.copy(ta.gc_manager); // be careful with gcm objects; they must contain a pointer to its tile array
  }

  // move constructor
  TileArray::TileArray (TileArray&& ta) :
  mpi_size(std::move(ta.mpi_size)), mpi_rank(std::move(ta.mpi_rank)), data_dim(std::move(ta.data_dim)),
  region_dim(std::move(ta.region_dim)), ghost_cell_dim(std::move(ta.ghost_cell_dim)), regions_data(std::move(ta.regions_data)),
  d_regions_data(std::move(ta.d_regions_data)), device_cache(std::move(ta.device_cache)), region_count(std::move(ta.region_count)),
  device_region_count(std::move(ta.device_region_count)), gc_manager(GhostCellManager(this)) {
    gc_manager.move(ta.gc_manager); // be careful with gcm objects; they must contain a pointer to its tile array
    ta.regions_data = nullptr;
    ta.d_regions_data = nullptr;
    ta.device_cache = nullptr;
    ta.region_count = 0;
    ta.device_region_count = 0;
  }

  // destructor
  TileArray::~TileArray () {
    if (regions_data != nullptr) {
      for (int i = 0; i < region_count; i++) {
        CUDA_CHECK(cudaFreeHost(regions_data[i]));
      }
      delete[] regions_data;
      if (device_region_count != 0) {
        for (int i = 0; i < device_region_count; i++) {
          CUDA_CHECK(cudaFree(d_regions_data[i]));
        }
        delete[] d_regions_data;
        delete[] device_cache;
        delete[] device_streams;
      }
    }
  }

  // copy assignment operator
  TileArray& TileArray::operator= (const TileArray& ta) {
    if (this != &ta) {
      // free existing resource
      if (regions_data != nullptr) {
        for (int i = 0; i < region_count; i++) {
          CUDA_CHECK(cudaFreeHost(regions_data[i]));
        }
        delete[] regions_data;
        if (device_region_count != 0) {
          for (int i = 0; i < device_region_count; i++) {
            CUDA_CHECK(cudaFree(d_regions_data[i]));
          }
          delete[] d_regions_data;
          delete[] device_cache;
          delete[] device_streams;
        }
      }
      // copy-swap idiom: please see implementation-notes.txt
      TileArray ta_copy(ta);
      std::swap(mpi_size, ta_copy.mpi_size);
      std::swap(mpi_rank, ta_copy.mpi_rank);
      std::swap(data_dim, ta_copy.data_dim);
      std::swap(region_dim,ta_copy.region_dim);
      std::swap(ghost_cell_dim, ta_copy.ghost_cell_dim);
      std::swap(regions_data, ta_copy.regions_data);
      std::swap(d_regions_data, ta_copy.d_regions_data);
      std::swap(device_cache, ta_copy.device_cache);
      std::swap(device_streams, ta_copy.device_streams);
      std::swap(region_count, ta_copy.region_count);
      std::swap(device_region_count, ta_copy.device_region_count);
      gc_manager.swap(ta_copy.gc_manager); // be careful with gcm objects; they must contain a pointer to its tile array
    }
    return *this;
  }

  // move assignment operator
  TileArray& TileArray::operator= (TileArray&& ta) {
    if (this != &ta) {
      // free existing resource
      if (regions_data != nullptr) {
        for (int i = 0; i < region_count; i++) {
          CUDA_CHECK(cudaFreeHost(regions_data[i]));
        }
        delete[] regions_data;
        if (device_region_count != 0) {
          for (int i = 0; i < device_region_count; i++) {
            CUDA_CHECK(cudaFree(d_regions_data[i]));
          }
          delete[] d_regions_data;
          delete[] device_cache;
          delete[] device_streams;
        }
      }
      // move
      mpi_size = std::move(ta.mpi_size);
      mpi_rank = std::move(ta.mpi_rank);
      data_dim = std::move(ta.data_dim);
      region_dim = std::move(ta.region_dim);
      ghost_cell_dim = std::move(ta.ghost_cell_dim);
      regions_data = std::move(ta.regions_data);
      d_regions_data = std::move(ta.d_regions_data);
      device_cache = std::move(ta.device_cache);
      device_streams = std::move(ta.device_streams);
      region_count = std::move(ta.region_count);
      device_region_count = std::move(ta.device_region_count);
      gc_manager.move(ta.gc_manager); // be careful with gcm objects; they must contain a pointer to its tile array
      ta.regions_data = nullptr;
      ta.d_regions_data = nullptr;
      ta.device_cache = nullptr;
      ta.device_streams = nullptr;
      ta.region_count = 0;
      ta.device_region_count = 0;
    }
    return *this;
  }

  void TileArray::printRegions () {
    std::cout << "Printing all the regions..." << std::endl;
    std::cout << "Warning all the regions are being transferred to host!" << std::endl;
    int ex_reg_dim_z = region_dim(0) + (ghost_cell_dim(0) * 2);
    int ex_reg_dim_y = region_dim(1) + (ghost_cell_dim(1) * 2);
    int ex_reg_dim_x = region_dim(2) + (ghost_cell_dim(2) * 2);
    for (int r = 0; r < region_count; r++) {
      double* region_data = regions_data[r];
      if (device_region_count > 0 && device_cache[r % device_region_count] == r) region_data = getRegionHostPtr(r);
      std::cout << "Region " << r << ": " << std::endl;
      for (int i = 0; i < ex_reg_dim_z; i++) {
        for (int j = 0; j < ex_reg_dim_y; j++) {
          for (int k = 0; k < ex_reg_dim_x; k++) {
            std::cout << region_data[(i * ex_reg_dim_y + j) * ex_reg_dim_x + k] << ", ";
          }
          std::cout << "\n" << std::endl;
        }
        std::cout << "\n" << std::endl;
      }
    }
  }

  void TileArray::updateGhostCells () {
    bool rdma_support = (getenv("MPICH_RDMA_ENABLED_CUDA") != NULL) && (atoi(getenv("MPICH_RDMA_ENABLED_CUDA")) == 1);
    bool are_all_on_host = true;
    int mpi_recv_req_count = 0;
    int cuda_transfer_event_count = 0;
    int ghost_zone_count = gc_manager.ghost_zone_count;
    int** cor_reg_indices = gc_manager.cor_reg_indices;
    for (int r = 0; r < region_count; r++) {
      bool is_r_on_device = false;
      if (device_region_count != 0) {
        int r_device_index = r % device_region_count;
        is_r_on_device = device_cache[r_device_index] == r;
      }
      for (int gz = 0; gz < ghost_zone_count; gz++) {
        int corresponding_process = cor_reg_indices[r][gz] / region_count; // compiler likely ...
        int corresponding_region = cor_reg_indices[r][gz] % region_count; // compiler likely ...
        if (corresponding_process != mpi_rank) { // ghost zone is from a region that is on a different process
          mpi_recv_req_count++;
          if (is_r_on_device) are_all_on_host = false;
        } else { // ghost zone is from a region that is on the same process
          bool is_cr_on_device = false;
          if (device_region_count != 0) {
            is_cr_on_device = device_cache[corresponding_region % device_region_count] == corresponding_region;
          }
          if (is_r_on_device && is_cr_on_device) {
            are_all_on_host = false;
          } else if (is_r_on_device && !is_cr_on_device) {
            cuda_transfer_event_count++;
            are_all_on_host = false;
          } else if (!is_r_on_device && is_cr_on_device) {
            are_all_on_host = false;
          }
        }
      }
    }
    are_all_on_host ? gc_manager.allHostUpdate(mpi_recv_req_count) : gc_manager.deviceInvolvedUpdate(rdma_support, mpi_recv_req_count, cuda_transfer_event_count);
  }

  // iterator class definitions
  TileArray::Iterator::Iterator (TileArray* ta, const int region_index, const Dimension& tile_dim, const float gpu_exec_ratio) :
  ta(ta), region_index(region_index), tile_index(0), tile_dim(tile_dim), gpu_exec_ratio(gpu_exec_ratio) {
    if (gpu_exec_ratio < 0 || gpu_exec_ratio > 1) throw TileArray::Iterator::InvalidGPUExecutionRatio();
  }

  Tile TileArray::Iterator::operator* () const {
    double* data;
    int device_id;
    int region_count = ta->device_region_count;
    if (region_index < (int) (region_count * gpu_exec_ratio)) {
      data = ta->getRegionDevicePtr(region_index);
      device_id = region_index % region_count;
    } else {
      data = ta->getRegionHostPtr(region_index);
      device_id = -1;
    }
    // following part does not need this branching, nevertheless it does branch because it is more optimized for gpu and/or no-tiling execution
    if (gpu_exec_ratio || tile_dim == ta->region_dim) {
      int id = ta->mpi_rank * ta->region_count + region_index;
      Dimension tile_low = ta->getRegionsLow();
      Dimension tile_high = ta->getRegionsHigh();
      return Tile(data, id, ta->getRegionsLow(), ta->getRegionsHigh(), ta->getRegionsPhysDim(), device_id);
    } else { // this part normally is the generalized part
      int id = (ta->mpi_rank * ta->region_count + region_index) * getTileCount() + tile_index;
      Dimension tile_count = ta->region_dim / tile_dim;
      int tile_count_y = tile_count(1);	int tile_count_x = tile_count(2);
      int tile_count_xy = tile_count_y * tile_count_x;
      int tile_z = tile_index / tile_count_xy;
      int remaining = tile_index % tile_count_xy; // compiler likely uses remainder of previous division (see implementation notes)
      int tile_y = remaining / tile_count_x;
      int tile_x = remaining % tile_count_x; // compiler likely uses remainder of previous division (see implementation notes)
      Dimension tile_low = ta->getRegionsLow() + (tile_dim * Dimension(tile_z, tile_y, tile_x));
      Dimension tile_high = (tile_low + tile_dim) - 1;
      return Tile(data, id, tile_low, tile_high, ta->getRegionsPhysDim(), device_id);
    }
  }

  TileArray::Iterator TileArray::Iterator::operator() (TileArray& ta) const {
    Iterator i = *this;
    i.ta = &ta;
    return i;
  }

  bool TileArray::Iterator::operator== (const Iterator& i) const {
    return region_index==i.region_index && tile_index==i.tile_index && ta==i.ta && tile_dim==i.tile_dim
    && (region_index==ta->region_count || gpu_exec_ratio==i.gpu_exec_ratio);
    // takes advantage of the order of evaluation - last check: if it is the end iterator, dont check its gpu_execution
  }

  TileArray::Iterator& TileArray::Iterator::operator++ () {
    // following part does not need this branching, nevertheless it does branch because it is more optimized for gpu and/or no-tiling execution
    if (gpu_exec_ratio || tile_dim == ta->region_dim) {
      region_index++;
    } else { // this part normally is the generalized part
      tile_index++;
      if (tile_index % getTileCount() == 0) {
        tile_index = 0;
        region_index++;
        if (region_index == ta->region_count) { // make it end iterator
          tile_dim = ta->region_dim;
        }
      }
    }
    return *this;
  }

  int TileArray::Iterator::getTileCount () const {
    Dimension tile_count_dim = ta->region_dim / tile_dim;
    return tile_count_dim(0) * tile_count_dim(1) * tile_count_dim(2);
  }

  TileArray::Iterator TileArray::begin () {
    is_data_initialized = true;
    return Iterator(this, 0, region_dim);
  }

  TileArray::Iterator TileArray::begin (const float gpu_exec_ratio) {
    if (gpu_exec_ratio && device_region_count == 0) { // init device components if not already
      MPI_Comm loc_comm;
      MPI_Comm_split_type(MPI_COMM_WORLD, OMPI_COMM_TYPE_HWTHREAD, mpi_rank, MPI_INFO_NULL, &loc_comm);
      int node_size;
      int node_rank;
      MPI_Comm_size(loc_comm, &node_size);
      MPI_Comm_rank(loc_comm, &node_rank);
      MPI_Comm_free(&loc_comm);
      int device_count;
      CUDA_CHECK(cudaGetDeviceCount(&device_count));
      if (device_count < node_size) throw TileArray::InsufficientDeviceCountException(); // another check is done while allocation GPU memory for buffers
      CUDA_CHECK(cudaSetDevice(node_rank));
      size_t region_phys_size = getRegionsPhysCount() * sizeof(double);
      size_t free_device_mem = 0;
      CUDA_CHECK(cudaMemGetInfo(&free_device_mem, nullptr));
      device_region_count = free_device_mem / region_phys_size;
      if (device_region_count > region_count) device_region_count = region_count;
      d_regions_data = new double*[device_region_count];
      device_cache = new int[device_region_count];
      int dev_reg_status;
      if (is_data_initialized) dev_reg_status = TIDA_DEVICE_CACHE_NO_REGION;
      else dev_reg_status = TIDA_DEVICE_CACHE_NOT_INIT_REGION;
      for (int i = 0; i < device_region_count; i++) {
        double* r;
        CUDA_CHECK(cudaMalloc((void **)&r, region_phys_size));
        d_regions_data[i] = r;
        device_cache[i] = dev_reg_status;
      }
      device_streams = new cudaStream_t[device_region_count];
      for (size_t i = 0; i < device_region_count; i++) {
        device_streams[i] = (cudaStream_t) acc_get_cuda_stream(i);
      }
      gc_manager.allocateDeviceBuffers();
    }
    is_data_initialized = true;
    return Iterator(this, 0, region_dim, gpu_exec_ratio);
  }

  TileArray::Iterator TileArray::begin (const Dimension& tile_dim) {
    if (region_dim % tile_dim != Dimension(0, 0, 0)) throw InvalidTileDimensionsException();
    is_data_initialized = true;
    return Iterator(this, 0, tile_dim);
  }

  TileArray::Iterator TileArray::end () {
    return Iterator(this, region_count, region_dim);
  }

  int TileArray::getRegionsPhysCount () const {
    Dimension regions_phys_dim = region_dim + (ghost_cell_dim * 2);
    return regions_phys_dim(0) * regions_phys_dim(1) * regions_phys_dim(2);
  }

  double* TileArray::getRegionHostPtr (const int region_index) {
    if (device_region_count != 0) {
      int region_device_index = region_index % device_region_count;
      if (device_cache[region_device_index] == region_index) {
        cudaStream_t& region_stream = device_streams[region_device_index];
        CUDA_CHECK(cudaMemcpyAsync(regions_data[region_index], d_regions_data[region_device_index], getRegionsPhysCount()*sizeof(double), cudaMemcpyDeviceToHost, region_stream));
        CUDA_CHECK(cudaStreamSynchronize(region_stream));
      }
      device_cache[region_device_index] = TIDA_DEVICE_CACHE_NO_REGION; // this line is here because the cache may have value of TIDA_DEVICE_CACHE_NOT_INIT_REGION
    }
    return regions_data[region_index];
  }

  double* TileArray::getRegionDevicePtr (const int region_index) {
    int region_device_index = region_index % device_region_count;
    int index_of_region_on_device = device_cache[region_device_index];
    if (index_of_region_on_device == region_index) { // for performance check this first
      return d_regions_data[region_device_index];
    } else if (index_of_region_on_device == TIDA_DEVICE_CACHE_NO_REGION) { // some other region is using assigned device memory, so move it to host
      size_t region_phys_size = getRegionsPhysCount() * sizeof(double);
      CUDA_CHECK(cudaMemcpyAsync(d_regions_data[region_device_index], regions_data[region_index], region_phys_size, cudaMemcpyHostToDevice, device_streams[region_device_index]));
      device_cache[region_device_index] = region_index;
      return d_regions_data[region_device_index];
    } else if (index_of_region_on_device == TIDA_DEVICE_CACHE_NOT_INIT_REGION) { // for lazy Initialization
      device_cache[region_device_index] = region_index;
      return d_regions_data[region_device_index];
    } else { // some other region is using assigned device memory, so move it to host
      size_t region_phys_size = getRegionsPhysCount() * sizeof(double);
      cudaStream_t& region_stream = device_streams[region_device_index];
      CUDA_CHECK(cudaMemcpyAsync(regions_data[index_of_region_on_device], d_regions_data[region_device_index], region_phys_size, cudaMemcpyDeviceToHost, region_stream));
      CUDA_CHECK(cudaMemcpyAsync(d_regions_data[region_device_index], regions_data[region_index], region_phys_size, cudaMemcpyHostToDevice, region_stream));
      device_cache[region_device_index] = region_index;
      return d_regions_data[region_device_index];
    }
  }

  // ghost cell manager class definitions
  TileArray::GhostCellManager::GhostCellManager (TileArray* ta) :  ta(ta) {}

  TileArray::GhostCellManager::~GhostCellManager () {
    int region_count = ta->region_count;
    if(ghost_zone_indices != nullptr) {
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          delete[] ghost_zone_indices[i][j];
        }
        delete[] ghost_zone_indices[i];
      }
      delete[] ghost_zone_indices;
    }
    if (cor_reg_indices != nullptr) {
      for (int i = 0; i < region_count; i++) {
        delete[] cor_reg_indices[i];
      }
      delete[] cor_reg_indices;
    }
    if (buffers != nullptr) {
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          if(zone_cell_counts[j] > 0) {
            CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_SEND_BUF]));
            CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_RECV_BUF]));
          }
          delete[] buffers[i][j];
        }
        delete[] buffers[i];
      }
      delete[] buffers;
    }
    if (d_buffers != nullptr) {
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          if(zone_cell_counts[j] > 0) {
            CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_SEND_BUF]));
            CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_RECV_BUF]));
          }
          delete[] d_buffers[i][j];
        }
        delete[] d_buffers[i];
      }
      delete[] d_buffers;
    }
    delete[] zone_cell_counts;
  }

  void TileArray::GhostCellManager::init (const bool include_corners, const int ghost_cell_update_option) {
    include_corners ? ghost_zone_count = 26 : ghost_zone_count = 6;
    Dimension** region_gz_indices = new Dimension*[ghost_zone_count];
    for (int i = 0; i < ghost_zone_count; i++) {
      region_gz_indices[i] = new Dimension[6]; // 6: src_low, src_high, dst_low, dst_high, counter_zone_src_low, counter_zone_src_high
    }
    zone_cell_counts = new int[ghost_zone_count];
    int region_count = ta->region_count;
    cor_reg_indices = new int*[region_count];
    // compute lows and highs for default option
    Dimension region_dim = ta->region_dim;
    Dimension ghost_cell_dim = ta->ghost_cell_dim;
    Dimension region_low = ta->getRegionsLow();
    Dimension region_high = ta->getRegionsHigh();
    int ghost_no = 0;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        for (int k = -1; k <= 1; k++) {
          int sum_abs = abs(i) + abs(j) + abs(k);
          if (sum_abs != 0 && (include_corners || sum_abs == 1)) {
            /*Dimension r = Dimension(i, j, k);
            Dimension dst_low = ((r - 1) / 2) * ghost_cell_dim + ((r + 1) / 2) * region_dim;
            Dimension dst_high = ((r - 1) / 2) * region_dim + ((r + 1) / 2) * ghost_cell_dim;*/
            Dimension r_m = (Dimension(i, j, k) - 1 ) / 2;
            Dimension r_p = (Dimension(i, j, k) + 1 ) / 2;
            Dimension dst_low = r_m * ghost_cell_dim + r_p * region_dim;
            Dimension dst_high = r_m * region_dim + r_p * ghost_cell_dim;
            region_gz_indices[ghost_no][TIDA_DST_LOW] = dst_low + region_low; // don't forget to add region low
            region_gz_indices[ghost_no][TIDA_DST_HIGH] = dst_high + region_high; // don't forget to add region high
            Dimension ghost_dim = region_gz_indices[ghost_no][TIDA_DST_HIGH] - region_gz_indices[ghost_no][TIDA_DST_LOW] + 1;
            zone_cell_counts[ghost_no] = ghost_dim(0) * ghost_dim(1) * ghost_dim(2);
            Dimension src_low = dst_low % region_dim;
            Dimension src_high = dst_high % region_dim;
            int slo[3];
            int shi[3];
            for (int d = 0; d < 3; d++) {
              (src_low(d) < 0) ? slo[d] = src_low(d) + region_dim(d) : slo[d] = src_low(d);
              (src_high(d) < 0) ? shi[d] = src_high(d) + region_dim(d) : shi[d] = src_high(d);
            }
            src_low = Dimension(slo[0], slo[1], slo[2]) + region_low;
            src_high = Dimension(shi[0], shi[1], shi[2]) + region_high;
            for (int d = 0; d < 3; d++) {
              (src_low(d) > region_dim(d)) ? slo[d] = src_low(d) % region_dim(d) : slo[d] = src_low(d);
              (src_high(d) > region_dim(d)) ? shi[d] = src_high(d) % region_dim(d) : shi[d] = src_high(d);
            }
            src_low = Dimension(slo[0], slo[1], slo[2]);
            src_high = Dimension(shi[0], shi[1], shi[2]);
            region_gz_indices[ghost_no][TIDA_SRC_LOW] = src_low;
            region_gz_indices[ghost_no][TIDA_SRC_HIGH] = src_high;
            region_gz_indices[getCounterGhostZone(ghost_no)][TIDA_COUNTER_SRC_LOW] = src_low;
            region_gz_indices[getCounterGhostZone(ghost_no)][TIDA_COUNTER_SRC_HIGH] = src_high;
            ghost_no++;
          }
        }
      }
    }
    ghost_zone_indices = new Dimension**[region_count];
    // all regions share the same ghost zone indices in default option
    for(int i = 0; i < region_count - 1; i++){
      ghost_zone_indices[i] = new Dimension*[ghost_zone_count];
      for (int j = 0; j < ghost_zone_count; j++) {
        ghost_zone_indices[i][j] = new Dimension[6];
        std::memcpy(ghost_zone_indices[i][j], region_gz_indices[j], 6*sizeof(Dimension));
      }
    }
    ghost_zone_indices[region_count - 1] = region_gz_indices;
    // compute indices based on default option, later change them if some other option is chosen
    Dimension region_count_dim = ta->data_dim / region_dim;
    int mpi_rank = ta->mpi_rank;
    int region_count_z = region_count_dim(0);
    int region_count_y = region_count_dim(1);
    int region_count_x = region_count_dim(2);
    int region_count_xy = region_count_y * region_count_x;
    for (int r = 0; r < region_count; r++) {
      int* rgn_cor_reg_indices = new int[ghost_zone_count];
      int r_index = mpi_rank * region_count + r;
      int region_z = r_index / region_count_xy;
      int remaining = r_index % region_count_xy; // compiler likely uses remainder of previous division (see implementation notes)
      int region_y = remaining / region_count_x;
      int region_x = remaining % region_count_x; // compiler likely uses remainder of previous division (see implementation notes)
      int ghost_no = 0;
      for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
          for (int k = -1; k <= 1; k++) {
            int sum_abs = abs(i) + abs(j) + abs(k);
            if (sum_abs != 0 && (include_corners || sum_abs == 1)) {
              int neighbour_x = region_x + k;
              int neighbour_y = region_y + j;
              int neighbour_z = region_z + i;
              if (neighbour_x == region_count_x) neighbour_x = 0;
              if (neighbour_x == -1) neighbour_x = region_count_x - 1;
              if (neighbour_y == region_count_y) neighbour_y = 0;
              if (neighbour_y == -1) neighbour_y = region_count_y - 1;
              if (neighbour_z == region_count_z) neighbour_z = 0;
              if (neighbour_z == -1) neighbour_z = region_count_z - 1;
              int src_index = neighbour_z * region_count_xy + neighbour_y * region_count_x + neighbour_x;
              rgn_cor_reg_indices[ghost_no] = src_index;
              ghost_no++;
            }
          }
        }
      }
      cor_reg_indices[r] = rgn_cor_reg_indices;
    }
    if (ghost_cell_update_option == TiDA::MIRROR_BOUND_UPDATE) {
      // update lows, highs and region indices for mirror boundary option - the difference from the default option is the regions on the boundaries will update ghost cells from themselves not from ...
      for (int r = 0; r < region_count; r++) {
        int r_index = mpi_rank * region_count + r;
        int region_z = r_index / region_count_xy;
        int remaining = r_index % region_count_xy; // compiler likely uses remainder of previous division (see implementation notes)
        int region_y = remaining / region_count_x;
        int region_x = remaining % region_count_x; // compiler likely uses remainder of previous division (see implementation notes)
        int ghost_no = 0;
        for (int i = -1; i <= 1; i++) {
          for (int j = -1; j <= 1; j++) {
            for (int k = -1; k <= 1; k++) {
              int sum_abs = abs(i) + abs(j) + abs(k);
              if (sum_abs != 0 && (include_corners || sum_abs == 1)) {
                int neighbour_x = region_x + k;
                int neighbour_y = region_y + j;
                int neighbour_z = region_z + i;
                bool need_update = false;
                if (neighbour_x == region_count_x) {
                  neighbour_x = region_x;
                  need_update = true;
                }
                if (neighbour_x == -1) {
                  neighbour_x = region_x;
                  need_update = true;
                }
                if (neighbour_y == region_count_y) {
                  neighbour_y = region_y;
                  need_update = true;
                }
                if (neighbour_y == -1) {
                  neighbour_y = region_y;
                  need_update = true;
                }
                if (neighbour_z == region_count_z) {
                  neighbour_z = region_z;
                  need_update = true;
                }
                if (neighbour_z == -1) {
                  neighbour_z = region_z;
                  need_update = true;
                }
                if (need_update) {
                  int src_index = neighbour_z * region_count_xy + neighbour_y * region_count_x + neighbour_x;
                  cor_reg_indices[r][ghost_no] = src_index;
                  Dimension ijk(i, j, k);
                  // ghost_zone_indices[r][ghost_no][TIDA_SRC_LOW] = ghost_zone_indices[r][ghost_no][TIDA_SRC_LOW] + (ijk * (region_dim - ghost_cell_dim - 1)); // this -1 is there because of cardiac sim, normally may not be needed;
                  // ghost_zone_indices[r][ghost_no][TIDA_SRC_HIGH] = ghost_zone_indices[r][ghost_no][TIDA_SRC_HIGH] + (ijk * (region_dim - ghost_cell_dim - 1)); // this -1 is there because of cardiac sim, normally may not be needed;
                  Dimension old_src_low = ghost_zone_indices[r][ghost_no][TIDA_SRC_LOW];
                  Dimension old_src_high = ghost_zone_indices[r][ghost_no][TIDA_SRC_HIGH];
                  ghost_zone_indices[r][ghost_no][TIDA_SRC_LOW] = ghost_zone_indices[r][ghost_no][TIDA_COUNTER_SRC_LOW] - ijk; // this -ijk is there because of cardiac sim, normally may not be needed;
                  ghost_zone_indices[r][ghost_no][TIDA_SRC_HIGH] = ghost_zone_indices[r][ghost_no][TIDA_COUNTER_SRC_HIGH] - ijk; // this -ijk is there because of cardiac sim, normally may not be needed;
                  if (src_index == r_index) {
                    ghost_zone_indices[r][ghost_no][TIDA_COUNTER_SRC_LOW] = old_src_low - ijk;
                    ghost_zone_indices[r][ghost_no][TIDA_COUNTER_SRC_HIGH] = old_src_high - ijk;
                  }
                }
                ghost_no++;
              }
            }
          }
        }
      }
    }
    // allocate buffers
    buffers = new double***[region_count];
    for (int i = 0; i < region_count; i++) {
      buffers[i] = new double**[ghost_zone_count];
      for (int j = 0; j < ghost_zone_count; j++) {
        buffers[i][j] = new double*[2]; // size of 2; one for pointer to send buffer and the other for receive buffer
        if(zone_cell_counts[j] > 0) {
          size_t size_of_buffers = zone_cell_counts[j]*sizeof(double);
          CUDA_CHECK(cudaMallocHost((void**)&buffers[i][j][TIDA_SEND_BUF], size_of_buffers));
          CUDA_CHECK(cudaMallocHost((void**)&buffers[i][j][TIDA_RECV_BUF], size_of_buffers));
        }
      }
    }
  }

  void TileArray::GhostCellManager::copy (const GhostCellManager& gcm) { // todo: test this
    int region_count = ta->region_count;
    if (this != &gcm) {
      // free existing resources
      if(ghost_zone_indices != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            delete[] ghost_zone_indices[i][j];
          }
          delete[] ghost_zone_indices[i];
        }
        delete[] ghost_zone_indices;
      }
      if (cor_reg_indices != nullptr) {
        for (int i = 0; i < region_count; i++) {
          delete[] cor_reg_indices[i];
        }
        delete[] cor_reg_indices;
      }
      if (buffers != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            if(zone_cell_counts[j] > 0) {
              CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_SEND_BUF]));
              CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_RECV_BUF]));
            }
            delete[] buffers[i][j];
          }
          delete[] buffers[i];
        }
        delete[] buffers;
      }
      if (d_buffers != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            if(zone_cell_counts[j] > 0) {
              CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_SEND_BUF]));
              CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_RECV_BUF]));
            }
            delete[] d_buffers[i][j];
          }
          delete[] d_buffers[i];
        }
        delete[] d_buffers;
      }
      delete[] zone_cell_counts;
      // copy
      ghost_zone_count = gcm.ghost_zone_count;
      ghost_zone_indices = new Dimension**[region_count];

      for (size_t i = 0; i < region_count; i++) {
        ghost_zone_indices[i] = new Dimension*[ghost_zone_count];
        for (int j = 0; j < ghost_zone_count; j++) {
          ghost_zone_indices[i][j] = new Dimension[6];
          std::memcpy(ghost_zone_indices[i][j], gcm.ghost_zone_indices[i][j], 6*sizeof(Dimension));
        }
      }

      zone_cell_counts = new int[ghost_zone_count];
      std::memcpy(zone_cell_counts, gcm.zone_cell_counts, ghost_zone_count*sizeof(int));
      cor_reg_indices = new int*[region_count];
      for (int i = 0; i < region_count; i++) {
        cor_reg_indices[i] = new int[ghost_zone_count];
        std::memcpy(cor_reg_indices[i], gcm.cor_reg_indices[i], ghost_zone_count*sizeof(int));
      }
      buffers = new double***[region_count];
      for (int i = 0; i < region_count; i++) {
        buffers[i] = new double**[ghost_zone_count];
        for (int j = 0; j < ghost_zone_count; j++) {
          buffers[i][j] = new double*[2]; // size of 2; one for pointer to send buffer and the other for receive buffer
          if(zone_cell_counts[j] > 0) {
            size_t size_of_buffers = zone_cell_counts[j]*sizeof(double);
            CUDA_CHECK(cudaMallocHost((void**)&buffers[i][j][TIDA_SEND_BUF], size_of_buffers));
            CUDA_CHECK(cudaMemcpy(buffers[i][j][TIDA_SEND_BUF], gcm.buffers[i][j][TIDA_SEND_BUF], size_of_buffers, cudaMemcpyHostToHost));
            CUDA_CHECK(cudaMallocHost((void**)&buffers[i][j][TIDA_RECV_BUF], size_of_buffers));
            CUDA_CHECK(cudaMemcpy(buffers[i][j][TIDA_RECV_BUF], gcm.buffers[i][j][TIDA_RECV_BUF], size_of_buffers, cudaMemcpyHostToHost));
          }
        }
      }
      if (d_buffers != nullptr) {
        d_buffers = new double***[region_count];
        for (int i = 0; i < region_count; i++) {
          d_buffers[i] = new double**[ghost_zone_count];
          for (int j = 0; j < ghost_zone_count; j++) {
            d_buffers[i][j] = new double*[2]; // size of 2; one for pointer to send buffer and the other for receive buffer
            if(zone_cell_counts[j] > 0) {
              size_t size_of_buffers = zone_cell_counts[j]*sizeof(double);
              CUDA_CHECK(cudaMalloc((void**)&d_buffers[i][j][TIDA_SEND_BUF], size_of_buffers));
              CUDA_CHECK(cudaMemcpy(d_buffers[i][j][TIDA_SEND_BUF], gcm.d_buffers[i][j][TIDA_SEND_BUF], size_of_buffers, cudaMemcpyDeviceToDevice));
              CUDA_CHECK(cudaMalloc((void**)&d_buffers[i][j][TIDA_RECV_BUF], size_of_buffers));
              CUDA_CHECK(cudaMemcpy(d_buffers[i][j][TIDA_RECV_BUF], gcm.d_buffers[i][j][TIDA_RECV_BUF], size_of_buffers, cudaMemcpyDeviceToDevice));
            }
          }
        }
      }
    }
  }

  void TileArray::GhostCellManager::move (GhostCellManager& gcm) {
    if (this != &gcm) {
      // free existing resources
      int region_count = ta->region_count;
      if(ghost_zone_indices != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            delete[] ghost_zone_indices[i][j];
          }
          delete[] ghost_zone_indices[i];
        }
        delete[] ghost_zone_indices;
      }
      if (cor_reg_indices != nullptr) {
        for (int i = 0; i < region_count; i++) {
          delete[] cor_reg_indices[i];
        }
        delete[] cor_reg_indices;
      }
      if (buffers != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            if(zone_cell_counts[j] > 0) {
              CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_SEND_BUF]));
              CUDA_CHECK(cudaFreeHost(buffers[i][j][TIDA_RECV_BUF]));
            }
            delete[] buffers[i][j];
          }
          delete[] buffers[i];
        }
        delete[] buffers;
      }
      if (d_buffers != nullptr) {
        for (int i = 0; i < region_count; i++) {
          for (int j = 0; j < ghost_zone_count; j++) {
            if(zone_cell_counts[j] > 0) {
              CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_SEND_BUF]));
              CUDA_CHECK(cudaFree(d_buffers[i][j][TIDA_RECV_BUF]));
            }
            delete[] d_buffers[i][j];
          }
          delete[] d_buffers[i];
        }
        delete[] d_buffers;
      }
      delete[] zone_cell_counts;
      // move
      ghost_zone_count = std::move(gcm.ghost_zone_count);
      ghost_zone_indices = std::move(gcm.ghost_zone_indices);
      zone_cell_counts = std::move(gcm.zone_cell_counts);
      cor_reg_indices = std::move(gcm.cor_reg_indices);
      buffers = std::move(gcm.buffers);
      d_buffers = std::move(gcm.d_buffers);
      ghost_zone_count = 0;
      gcm.ghost_zone_indices = nullptr;
      gcm.zone_cell_counts = nullptr;
      gcm.cor_reg_indices = nullptr;
      gcm.buffers = nullptr;
      gcm.d_buffers = nullptr;
    }
  }

  void TileArray::GhostCellManager::swap (GhostCellManager& gcm) {
    std::swap(ghost_zone_count, gcm.ghost_zone_count);
    std::swap(ghost_zone_indices, gcm.ghost_zone_indices);
    std::swap(zone_cell_counts, gcm.zone_cell_counts);
    std::swap(cor_reg_indices, gcm.cor_reg_indices);
    std::swap(buffers, gcm.buffers);
    std::swap(d_buffers, gcm.d_buffers);
  }

  void TileArray::GhostCellManager::allocateDeviceBuffers () {
    // check this -> check if there is sufficient memory, if not throw exception
    int region_count = ta->region_count;
    d_buffers = new double***[region_count];
    for (int i = 0; i < region_count; i++) {
      d_buffers[i] = new double**[ghost_zone_count];
      for (int j = 0; j < ghost_zone_count; j++) {
        d_buffers[i][j] = new double*[2]; // size of 2; one for pointer to send buffer and the other for receive buffer
        if(zone_cell_counts[j] > 0) {
          size_t size_of_buffers = zone_cell_counts[j]*sizeof(double);
          CUDA_CHECK(cudaMalloc((void**)&d_buffers[i][j][TIDA_SEND_BUF], size_of_buffers));
          CUDA_CHECK(cudaMalloc((void**)&d_buffers[i][j][TIDA_RECV_BUF], size_of_buffers));
        }
      }
    }
  }

  int TileArray::GhostCellManager::getCounterGhostZone (int gz) {
    return ghost_zone_count - gz - 1;
  }

  void TileArray::GhostCellManager::allHostUpdate (const int mpi_recv_req_count) {
    int region_count = ta->region_count;
    int ex_reg_dim_y = ta->region_dim(1) + (ta->ghost_cell_dim(1) * 2);
    int ex_reg_dim_x = ta->region_dim(2) + (ta->ghost_cell_dim(2) * 2);
    if (ta->mpi_size == 1) { // there is only 1 process: tested - functions properly
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
          int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
          Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
          int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
          double* dst_region = ta->regions_data[i];
          Dimension src_low = ghost_zone_indices[i][j][TIDA_SRC_LOW];
          int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
          double* src_region = ta->regions_data[cor_reg_indices[i][j]];
          for (int dst_z = dl0, src_z = sl0; dst_z <= dh0; dst_z++, src_z++) {
            for (int dst_y = dl1, src_y = sl1; dst_y <= dh1; dst_y++, src_y++) {
              for (int dst_x = dl2, src_x = sl2; dst_x <= dh2; dst_x++, src_x++) {
                dst_region[(dst_z * ex_reg_dim_y + dst_y) * ex_reg_dim_x + dst_x] = src_region[(src_z * ex_reg_dim_y + src_y) * ex_reg_dim_x + src_x];
              }
            }
          }
        }
      }
    } else { // there are multiple processes: tested - functions properly
      MPI_Request recv_reqs[mpi_recv_req_count];
      int recv_req_no = 0;
      // first pack and send
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          int corresponding_process = cor_reg_indices[i][j] / region_count;
          int corresponding_region = cor_reg_indices[i][j] % region_count;
          if (corresponding_process != ta->mpi_rank) {
            // receive
            int recv_tag = i * ghost_zone_count + j;
            double* recv_buf = buffers[i][j][TIDA_RECV_BUF];
            MPI_Irecv(recv_buf, zone_cell_counts[j], MPI_DOUBLE, corresponding_process, recv_tag, MPI_COMM_WORLD, &recv_reqs[recv_req_no]);
            recv_req_no++;
            // pack and send
            int counter_zone = getCounterGhostZone(j);
            int send_tag = corresponding_region * ghost_zone_count + counter_zone;
            MPI_Request send_req;
            double* send_buf = buffers[i][j][TIDA_SEND_BUF];
            Dimension src_low = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            Dimension src_high = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_HIGH];
            int sh0 = src_high(0); int sh1 = src_high(1); int sh2 = src_high(2);
            double* src_region = ta->regions_data[i];
            int buf_index = 0;
            for (int z = sl0; z <= sh0; z++) {
              for (int y = sl1; y <= sh1; y++) {
                for (int x = sl2; x <= sh2; x++) {
                  send_buf[buf_index] = src_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x];
                  buf_index++;
                }
              }
            }
            MPI_Isend(send_buf, zone_cell_counts[counter_zone], MPI_DOUBLE, corresponding_process, send_tag, MPI_COMM_WORLD, &send_req);
          }
        }
      }
      // second exchange cells for regions on the same process
      for (int i = 0; i < region_count; i++) {
        for (int j = 0; j < ghost_zone_count; j++) {
          int corresponding_process = cor_reg_indices[i][j] / region_count;
          if (corresponding_process == ta->mpi_rank) {
            Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            double* dst_region = ta->regions_data[i];
            Dimension src_low = ghost_zone_indices[i][j][TIDA_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            double* src_region = ta->regions_data[cor_reg_indices[i][j]%region_count];
            for (int dst_z = dl0, src_z = sl0; dst_z <= dh0; dst_z++, src_z++) {
              for (int dst_y = dl1, src_y = sl1; dst_y <= dh1; dst_y++, src_y++) {
                for (int dst_x = dl2, src_x = sl2; dst_x <= dh2; dst_x++, src_x++) {
                  dst_region[(dst_z * ex_reg_dim_y + dst_y) * ex_reg_dim_x + dst_x] = src_region[(src_z * ex_reg_dim_y + src_y) * ex_reg_dim_x + src_x];
                }
              }
            }
          }
        }
      }
      // third unpack cells from other processes
      int completed_req_count = 0;
      while (completed_req_count != mpi_recv_req_count) {
        for (int i = 0; i < mpi_recv_req_count; i++) {
          int is_completed = 0;
          MPI_Status status;
          MPI_Test(&recv_reqs[i], &is_completed, &status);
          if (is_completed && status.MPI_SOURCE != -1) {
            int recv_tag = status.MPI_TAG;
            int r = recv_tag / ghost_zone_count;
            int gz = recv_tag % ghost_zone_count;
            // unpack
            Dimension dst_low = ghost_zone_indices[r][gz][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[r][gz][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            double* dst_region = ta->regions_data[r];
            double* recv_buf = buffers[r][gz][TIDA_RECV_BUF];
            int buf_index = 0;
            for (int z = dl0; z <= dh0; z++) {
              for (int y = dl1; y <= dh1; y++) {
                for (int x = dl2; x <= dh2; x++) {
                  dst_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x] = recv_buf[buf_index];
                  buf_index++;
                }
              }
            }
            completed_req_count++;
          }
        }
      }
    }
  }

  void TileArray::GhostCellManager::deviceInvolvedUpdate (const bool rdma_support, const int mpi_recv_req_count, const int cuda_transfer_event_count) {
    int region_count = ta->region_count;
    int ex_reg_dim_y = ta->region_dim(1) + (ta->ghost_cell_dim(1) * 2);
    int ex_reg_dim_x = ta->region_dim(2) + (ta->ghost_cell_dim(2) * 2);
    int device_region_count = ta->device_region_count;
    cudaEvent_t transfer_events[cuda_transfer_event_count]; // necessary for consistency in transfers
    cudaEvent_t* stream_events; // necessary for consistency in exchange
    if (device_region_count > 0) {
      stream_events = new cudaEvent_t[device_region_count];
      for (int i = 0; i < device_region_count; i++) {
        cudaStream_t stream = (cudaStream_t) acc_get_cuda_stream(i);
        CUDA_CHECK(cudaEventCreateWithFlags(&stream_events[i], cudaEventDisableTiming)); // please see implementation notes for why a flag is used.
        CUDA_CHECK(cudaEventRecord(stream_events[i], stream));
      }
    }
    MPI_Request recv_reqs[mpi_recv_req_count];
    // phase I:
    // this phase is for ghost zones that need to be transfered/received to/from other processes
    // 1- initiate asynchronous mpi receives, 2- pack ghost zones to buffers, 3- send the ones on the host with mpi
    int recv_req_no = 0;
    for (int i = 0; i < region_count; i++) {
      int region_device_index = i % device_region_count;
      bool isIOnDevice = ta->device_cache[region_device_index] == i;
      for (int j = 0; j < ghost_zone_count; j++) {
        int corresponding_process = cor_reg_indices[i][j] / region_count; // compiler likely ...
        int corresponding_region = cor_reg_indices[i][j] % region_count; // compiler likely ...
        if (corresponding_process != ta->mpi_rank) { // ghost zone is from a region that is on a different process
          int recv_tag = i * ghost_zone_count + j;
          double* recv_buf;
          double* send_buf;
          if (isIOnDevice && rdma_support) {
            recv_buf = d_buffers[i][j][TIDA_RECV_BUF];
            send_buf = d_buffers[i][j][TIDA_SEND_BUF];
          } else {
            recv_buf = buffers[i][j][TIDA_RECV_BUF];
            send_buf = buffers[i][j][TIDA_SEND_BUF];
          }
          // receive
          MPI_Irecv(recv_buf, zone_cell_counts[j], MPI_DOUBLE, corresponding_process, recv_tag, MPI_COMM_WORLD, &recv_reqs[recv_req_no]);
          recv_req_no++;
          int counter_zone = getCounterGhostZone(j);
          Dimension src_low = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_LOW];
          int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
          Dimension src_high = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_HIGH];
          int sh0 = src_high(0); int sh1 = src_high(1); int sh2 = src_high(2);
          // pack
          if (isIOnDevice) { // pack on device ...
            double* buf;
            rdma_support ? buf = send_buf : buf = d_buffers[i][j][TIDA_SEND_BUF];
            double* src_region = ta->d_regions_data[region_device_index];
            #pragma acc parallel loop collapse(3) deviceptr(buf, src_region) async(region_device_index)
            for (int z = sl0; z <= sh0; z++) {
              for (int y = sl1; y <= sh1; y++) {
                for (int x = sl2; x <= sh2; x++) {
                  buf[((z-sl0) * (sh1-sl1+1) + (y-sl1)) * (sh2-sl2+1) + (x-sl2)] = src_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x];
                }
              }
            }
            if (!rdma_support) cudaMemcpyAsync(send_buf, buf, zone_cell_counts[counter_zone]*sizeof(double), cudaMemcpyDeviceToHost, ta->device_streams[region_device_index]);
            // do not just yet send the buffer, send it in phase II
          } else { // pack on host
            double* src_region = ta->regions_data[i];
            int buf_index = 0;
            for (int z = sl0; z <= sh0; z++) {
              for (int y = sl1; y <= sh1; y++) {
                for (int x = sl2; x <= sh2; x++) {
                  send_buf[buf_index] = src_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x];
                  buf_index++;
                }
              }
            }
            // send the buffer
            int send_tag = corresponding_region * ghost_zone_count + counter_zone;
            MPI_Request send_req;
            MPI_Isend(send_buf, zone_cell_counts[counter_zone], MPI_DOUBLE, corresponding_process, send_tag, MPI_COMM_WORLD, &send_req);
          }
        }
      }
    }
    // phase II:
    // this phase is for sending ghost zone buffers on device with mpi
    // 1- wait for completion of copy of ghost zones to buffers 2- send them with mpi
    for (int i = 0; i < region_count; i++) {
      int region_device_index = i % device_region_count;
      bool isIOnDevice = ta->device_cache[region_device_index] == i;
      if (isIOnDevice) {
        for (int j = 0; j < ghost_zone_count; j++) {
          int corresponding_process = cor_reg_indices[i][j] / region_count; // compiler likely ...
          int corresponding_region = cor_reg_indices[i][j] % region_count; // compiler likely ...
          if (corresponding_process != ta->mpi_rank) { // ghost zone is from a region that is on a different process
            double* send_buf;
            rdma_support ? send_buf = d_buffers[i][j][TIDA_SEND_BUF] : send_buf = buffers[i][j][TIDA_SEND_BUF];
            int send_tag = corresponding_region * ghost_zone_count + getCounterGhostZone(j);
            MPI_Request send_req;
            CUDA_CHECK(cudaStreamSynchronize(ta->device_streams[region_device_index])); // need to be sure that copy to buffer is completed before sending with MPI
            // send
            MPI_Isend(send_buf, zone_cell_counts[getCounterGhostZone(j)], MPI_DOUBLE, corresponding_process, send_tag, MPI_COMM_WORLD, &send_req);
          }
        }
      }
    }
    // phase III:
    // this phase is for ghost zones that need to be transfered between host and device in the same processes
    //
    int transfer_no = 0;
    for (int i = 0; i < region_count; i++) {
      int region_device_index = i % device_region_count;
      bool isIOnDevice = ta->device_cache[region_device_index] == i;
      for (int j = 0; j < ghost_zone_count; j++) {
        int corresponding_process = cor_reg_indices[i][j] / region_count; // compiler likely ...
        int corresponding_region = cor_reg_indices[i][j] % region_count; // compiler likely ...
        if (corresponding_process == ta->mpi_rank) { // ghost zone is from a region that is on the same process
          bool isNeighbourOnDevice = ta->device_cache[corresponding_region % device_region_count] == corresponding_region;
          if (isIOnDevice && !isNeighbourOnDevice) { // pack on device and send it to host
            size_t buf_size = zone_cell_counts[j] * sizeof(double);
            double* send_buf = d_buffers[i][j][TIDA_SEND_BUF];
            double* recv_buf = buffers[corresponding_region][getCounterGhostZone(j)][TIDA_RECV_BUF];
            double* src_region = ta->d_regions_data[region_device_index];
            Dimension src_low = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            Dimension src_high = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_HIGH];
            int sh0 = src_high(0); int sh1 = src_high(1); int sh2 = src_high(2);
            #pragma acc parallel loop collapse(3) deviceptr(send_buf, src_region) async(region_device_index)
            for (int z = sl0; z <= sh0; z++) {
              for (int y = sl1; y <= sh1; y++) {
                for (int x = sl2; x <= sh2; x++) {
                  send_buf[((z - sl0) * (sh1-sl1+1) + (y - sl1)) * (sh2-sl2+1) + (x - sl2)] = src_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x];
                }
              }
            }
            cudaStream_t& region_stream = ta->device_streams[region_device_index];
            CUDA_CHECK(cudaMemcpyAsync(recv_buf, send_buf, buf_size, cudaMemcpyDeviceToHost, region_stream));
            CUDA_CHECK(cudaEventCreateWithFlags(&transfer_events[transfer_no], cudaEventDisableTiming)); // please see implementation notes for why a flag is used.
            CUDA_CHECK(cudaEventRecord(transfer_events[transfer_no], region_stream));
            transfer_no++;
          } else if (!isIOnDevice && isNeighbourOnDevice) { // pack on host and send it to device
            size_t buf_size = zone_cell_counts[j] * sizeof(double);
            double* send_buf = buffers[i][j][TIDA_SEND_BUF];
            double* recv_buf = d_buffers[corresponding_region][getCounterGhostZone(j)][TIDA_RECV_BUF];
            double* src_region = ta->regions_data[i];
            Dimension src_low = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            Dimension src_high = ghost_zone_indices[i][j][TIDA_COUNTER_SRC_HIGH];
            int sh0 = src_high(0); int sh1 = src_high(1); int sh2 = src_high(2);
            int buf_index = 0;
            for (int z = sl0; z <= sh0; z++) {
              for (int y = sl1; y <= sh1; y++) {
                for (int x = sl2; x <= sh2; x++) {
                  send_buf[buf_index] = src_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x];
                  buf_index++;
                }
              }
            }
            region_device_index = corresponding_region % device_region_count; // use receivers device id
            CUDA_CHECK(cudaMemcpyAsync(recv_buf, send_buf, buf_size, cudaMemcpyHostToDevice, ta->device_streams[region_device_index]));
          }
        }
      }
    }
    // phase IV:
    // this phase is for ghost zones that need to be exchanged on device or host
    //
    for (int i = 0; i < region_count; i++) {
      int region_device_index = i % device_region_count;
      bool isIOnDevice = ta->device_cache[region_device_index] == i;
      for (int j = 0; j < ghost_zone_count; j++) {
        int corresponding_process = cor_reg_indices[i][j] / region_count;
        int corresponding_region = cor_reg_indices[i][j] % region_count; // compiler likely uses ...
        if (corresponding_process == ta->mpi_rank) {
          bool isNeighbourOnDevice = ta->device_cache[corresponding_region % device_region_count] == corresponding_region;
          if (isIOnDevice && isNeighbourOnDevice) { // exchange on device
            int device_src_index = corresponding_region % device_region_count;
            int device_dst_index = i % device_region_count;
            double* src_region = ta->d_regions_data[device_src_index];
            double* dst_region = ta->d_regions_data[device_dst_index];
            Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            Dimension src_low = ghost_zone_indices[i][j][TIDA_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            cudaStream_t dst_stream = (cudaStream_t) acc_get_cuda_stream(device_dst_index);
            CUDA_CHECK(cudaStreamWaitEvent(dst_stream, stream_events[device_src_index], 0));
            #pragma acc parallel loop collapse(3) deviceptr(src_region, dst_region) async(device_dst_index)
            for (int dst_z = dl0, src_z = sl0; dst_z <= dh0; dst_z++, src_z++) {
              for (int dst_y = dl1, src_y = sl1; dst_y <= dh1; dst_y++, src_y++) {
                for (int dst_x = dl2, src_x = sl2; dst_x <= dh2; dst_x++, src_x++) {
                  dst_region[(dst_z * ex_reg_dim_y + dst_y) * ex_reg_dim_x + dst_x] = src_region[(src_z * ex_reg_dim_y + src_y) * ex_reg_dim_x + src_x];
                }
              }
            }
          } else if (!isIOnDevice && !isNeighbourOnDevice) { // exchange on host
            double* dst_region = ta->regions_data[i];
            double* src_region = ta->regions_data[cor_reg_indices[i][j]%region_count];
            Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            Dimension src_low = ghost_zone_indices[i][j][TIDA_SRC_LOW];
            int sl0 = src_low(0); int sl1 = src_low(1); int sl2 = src_low(2);
            for (int dst_z = dl0, src_z = sl0; dst_z <= dh0; dst_z++, src_z++) {
              for (int dst_y = dl1, src_y = sl1; dst_y <= dh1; dst_y++, src_y++) {
                for (int dst_x = dl2, src_x = sl2; dst_x <= dh2; dst_x++, src_x++) {
                  dst_region[(dst_z * ex_reg_dim_y + dst_y) * ex_reg_dim_x + dst_x] = src_region[(src_z * ex_reg_dim_y + src_y) * ex_reg_dim_x + src_x];
                }
              }
            }
          }
        }
      }
    }
    // phase V:
    // unpack ghost zones received with mpi
    //
    int completed_req_count = 0;
    while (completed_req_count != mpi_recv_req_count) {
      for (int i = 0; i < mpi_recv_req_count; i++) {
        int is_completed = 0;
        MPI_Status status;
        MPI_Test(&recv_reqs[i], &is_completed, &status);
        if (is_completed && status.MPI_SOURCE != -1) {
          int recv_tag = status.MPI_TAG;
          int r = recv_tag / ghost_zone_count; // compiler likely uses ...
          int gz = recv_tag % ghost_zone_count; // compiler likely uses ...
          int r_device_index = r % device_region_count;
          bool is_r_on_device = ta->device_cache[r_device_index] == r;
          // unpack
          Dimension dst_low = ghost_zone_indices[r][gz][TIDA_DST_LOW];
          int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
          Dimension dst_high = ghost_zone_indices[r][gz][TIDA_DST_HIGH];
          int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
          double* recv_buf;
          (is_r_on_device && rdma_support) ? recv_buf = d_buffers[r][gz][TIDA_RECV_BUF] : recv_buf = buffers[r][gz][TIDA_RECV_BUF];
          if (is_r_on_device) { // unpack on device
            double* buf = recv_buf;
            if (!rdma_support) {
              buf = d_buffers[r][gz][TIDA_RECV_BUF];
              CUDA_CHECK(cudaMemcpyAsync(buf, recv_buf, zone_cell_counts[gz]*sizeof(double), cudaMemcpyHostToDevice, ta->device_streams[r_device_index]));
            }
            double* dst_region = ta->d_regions_data[r_device_index];
            #pragma acc parallel loop collapse(3) deviceptr(buf, dst_region) async(r_device_index)
            for (int z = dl0; z <= dh0; z++) {
              for (int y = dl1; y <= dh1; y++) {
                for (int x = dl2; x <= dh2; x++) {
                  dst_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x] = buf[((z - dl0) * (dh1-dl1+1) + (y - dl1)) * (dh2-dl2+1) + (x - dl2)];
                }
              }
            }
          } else { // unpack on host
            double* dst_region = ta->regions_data[r];
            double* recv_buf = buffers[r][gz][TIDA_RECV_BUF];
            int buf_index = 0;
            for (int z = dl0; z <= dh0; z++) {
              for (int y = dl1; y <= dh1; y++) {
                for (int x = dl2; x <= dh2; x++) {
                  dst_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x] = recv_buf[buf_index];
                  buf_index++;
                }
              }
            }
          }
          completed_req_count++;
        }
      }
    }
    // phase VI:
    // unpack ghost zones transfered between host and device in the same process
    //
    transfer_no = 0;
    for (int i = 0; i < region_count; i++) {
      int region_device_index = i % device_region_count;
      bool isIOnDevice = ta->device_cache[region_device_index] == i;
      for (int j = 0; j < ghost_zone_count; j++) {
        int corresponding_process = cor_reg_indices[i][j] / region_count; // compiler likely uses ...
        int corresponding_region = cor_reg_indices[i][j] % region_count; // compiler likely uses ...
        if (corresponding_process == ta->mpi_rank) { // ghost zone is from a region that is on the same process
          bool isNeighbourOnDevice = ta->device_cache[corresponding_region % device_region_count] == corresponding_region;
          if (isIOnDevice && !isNeighbourOnDevice) { // unpack on device
            double* recv_buf = d_buffers[i][j][TIDA_RECV_BUF];
            double* dst_region = ta->d_regions_data[region_device_index];
            Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            #pragma acc parallel loop collapse(3) deviceptr(recv_buf, dst_region) async(region_device_index)
            for (int z = dl0; z <= dh0; z++) {
              for (int y = dl1; y <= dh1; y++) {
                for (int x = dl2; x <= dh2; x++) {
                  dst_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x] = recv_buf[((z - dl0) * (dh1-dl1+1) + (y - dl1)) * (dh2-dl2+1) + (x - dl2)];
                }
              }
            }
          } else if (!isIOnDevice && isNeighbourOnDevice) { // sync and unpack on host
            double* recv_buf = buffers[i][j][TIDA_RECV_BUF];
            double* dst_region = ta->regions_data[i];
            Dimension dst_low = ghost_zone_indices[i][j][TIDA_DST_LOW];
            int dl0 = dst_low(0); int dl1 = dst_low(1); int dl2 = dst_low(2);
            Dimension dst_high = ghost_zone_indices[i][j][TIDA_DST_HIGH];
            int dh0 = dst_high(0); int dh1 = dst_high(1); int dh2 = dst_high(2);
            CUDA_CHECK(cudaEventSynchronize(transfer_events[transfer_no]));
            transfer_no++;
            int buf_index = 0;
            for (int z = dl0; z <= dh0; z++) {
              for (int y = dl1; y <= dh1; y++) {
                for (int x = dl2; x <= dh2; x++) {
                  dst_region[(z * ex_reg_dim_y + y) * ex_reg_dim_x + x] = recv_buf[buf_index];
                  buf_index++;
                }
              }
            }
          }
        }
      }
    }
    // free resources
    delete[] stream_events;
  }
