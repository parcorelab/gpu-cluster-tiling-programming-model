/*
* TiDA.h
*
* Burak Bastem
* Date
*
*/

#ifndef TIDA_H_
#define TIDA_H_

#include "TileArray.h"

class TiDA {
public:
  static const int DEFAULT_UPDATE = 0;
  static const int MIRROR_BOUND_UPDATE = 1;

  template<typename T> static void compute(Tile& tile, T&& function) {
    int low0 = tile.low(0); int low1 = tile.low(1); int low2 = tile.low(2);
    int high0 = tile.high(0); int high1 = tile.high(1); int high2 = tile.high(2);
    int reg_size_0 = tile.phys_dim(0); int reg_size_1 = tile.phys_dim(1); int reg_size_2 = tile.phys_dim(2);
    double* data = tile.data;
    if (tile.d_id == -1) {
      #pragma omp parallel for
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      } else {
        #pragma acc parallel loop collapse(3) deviceptr(data) async(tile.d_id)
        for(int i = low0; i <= high0; i++){
          for(int j = low1; j <= high1; j++){
            for(int k = low2; k <= high2; k++){
              function(data, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
            }
          }
        }
      }
    }
  }

  template<typename T> static void compute(Tile& tile_0, Tile& tile_1, T&& function) {
    int low0 = tile_0.low(0); int low1 = tile_0.low(1); int low2 = tile_0.low(2);
    int high0 = tile_0.high(0); int high1 = tile_0.high(1); int high2 = tile_0.high(2);
    int reg_size_0 = tile_0.phys_dim(0); int reg_size_1 = tile_0.phys_dim(1); int reg_size_2 = tile_0.phys_dim(2);
    double* data_0 = tile_0.data;
    double* data_1 = tile_1.data;
    if (tile_0.d_id == -1) {
      #pragma omp parallel for
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data_0, data_1, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      }
    } else {
      #pragma acc parallel loop collapse(3) deviceptr(data_0, data_1) async(tile_0.d_id)
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data_0, data_1, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      }
    }
  }

  template<typename T> static void compute(Tile& tile_0, Tile& tile_1, Tile& tile_2, T&& function) {
    int low0 = tile_0.low(0); int low1 = tile_0.low(1); int low2 = tile_0.low(2);
    int high0 = tile_0.high(0); int high1 = tile_0.high(1); int high2 = tile_0.high(2);
    int reg_size_0 = tile_0.phys_dim(0); int reg_size_1 = tile_0.phys_dim(1); int reg_size_2 = tile_0.phys_dim(2);
    double* data_0 = tile_0.data;
    double* data_1 = tile_1.data;
    double* data_2 = tile_2.data;
    if (tile_0.d_id == -1) {
      #pragma omp parallel for
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data_0, data_1, data_2, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      }
    } else {
      #pragma acc parallel loop collapse(3) deviceptr(data_0, data_1, data_2) async(tile_0.d_id)
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data_0, data_1, data_2, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      }
    }
  }

  template<typename T> static void compute_with_capture(Tile& tile, T&& function) {
    int low0 = tile.low(0); int low1 = tile.low(1); int low2 = tile.low(2);
    int high0 = tile.high(0); int high1 = tile.high(1); int high2 = tile.high(2);
    int reg_size_0 = tile.phys_dim(0); int reg_size_1 = tile.phys_dim(1); int reg_size_2 = tile.phys_dim(2);
    double* data = tile.data;
    if (tile.d_id == -1) {
      #pragma omp parallel for
      for(int i = low0; i <= high0; i++){
        for(int j = low1; j <= high1; j++){
          for(int k = low2; k <= high2; k++){
            function(data, reg_size_0, reg_size_1, reg_size_2, (i * reg_size_1 + j) * reg_size_2 + k);
          }
        }
      }
    }
  }
};

#endif /* TIDA_H_ */
