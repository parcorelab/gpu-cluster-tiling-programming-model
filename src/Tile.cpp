/*
* Tile.cpp
*
* Burak Bastem
* January 9, 2018
*
*/

#include "Tile.h"

// constructers
Tile::Tile(double* data, const int id, const Dimension& low, const Dimension& high, const Dimension& phys_dim, const int d_id = -1) :
	data(data),	id(id),	low(low),	high(high),	phys_dim(phys_dim), d_id(d_id) { }
