/*
 * Dimension.cpp
 *
 * Burak Bastem
 * January 9, 2018
 *
 */

#include "Dimension.h"

Dimension::Dimension (const int depth, const int height, const int width) {
    dims[0] = depth;
    dims[1] = height;
    dims[2] = width;
}

// other public functions
const int* Dimension::toArray () const {
  int* d = new int[3];
  d[0] = dims[0];
  d[1] = dims[1];
  d[2] = dims[2];
  return d;
}
