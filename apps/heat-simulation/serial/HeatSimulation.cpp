/*
 * HeatSimulation.cpp
 */

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include "Dimension.h"

using namespace std;

double getTime() {
	const double kMicro = 1.0e-6;
	struct timeval TV;
	struct timezone TZ;
	const int RC = gettimeofday(&TV, &TZ);
	if(RC == -1) {
		cout << "ERROR: Bad call to gettimeofday\n" << endl;
		return -1;
	}
	return( ((double)TV.tv_sec) + kMicro * ((double)TV.tv_usec));
}

void initData(double* data, const Dimension& dim) {
	for (int i = 0; i < dim(0); i++) {
		for (int j = 0; j < dim(1); j++) {
			for (int k = 0; k < dim(2); k++) {
				if (i == 0 || j == 0 || k == 0 || i == dim(0) - 1 || j == dim(1) - 1 || k == dim(2) - 1) {
					data[(i * dim(1) + j) * dim(2) + k] = 0;
				} else {
					data[(i * dim(1) + j) * dim(2) + k] = ((i-1) * (dim(1) - 2) + (j-1)) * (dim(2) - 2) + (k-1);
				}
			}
		}
	}
}

void compute(double* data, double* data_next, const Dimension& low, const Dimension& high, const Dimension& dim, const double alpha) {
	int l0 = low(0); int l1 = low(1);	int l2 = low(2);
	int h0 = high(0);	int h1 = high(1);	int h2 = high(2);
	int d1 = dim(1); int d2 = dim(2);
	// update boundaries
	// front
	for (int j = l1; j <= h1; j++) {
		for(int k = l2; k<= h2; k++) {
			data[j * d2 + k] = data[(h0 * d1 + j) * d2 + k];
		}
	}
	// back
	for (int j = l1; j <= h1; j++) {
		for(int k = l2; k<= h2; k++) {
			data[((h0 + 1) * d1 + j) * d2 + k] = data[(l1 * d1 + j) * d2 + k];
		}
	}
	// top
	for (int i = l1; i <= h0; i++) {
		for(int k = l2; k<= h2; k++) {
			data[(i * d1) * d2 + k] = data[(i * d1 + h1) * d2 + k];
		}
	}
	// bottom
	for (int i = l1; i <= h0; i++) {
		for(int k = l2; k<= h2; k++) {
			data[(i * d1 + (h1 + 1)) * d2 + k] = data[(i * d1 + l1) * d2 + k];
		}
	}
	// left
	for (int i = l1; i <= h0; i++) {
		for(int j = l1; j<= h1; j++) {
			data[(i * d1 + j) * d2] = data[(i * d1 + j) * d2 + h2];
		}
	}
	// right
	for (int i = l1; i <= h0; i++) {
		for(int j = l1; j<= h1; j++) {
			data[(i * d1 + j) * d2 + (h2 + 1)] = data[(i * d1 + j) * d2 + l2];
		}
	}
	// do stencil
	for (int i = l0; i <= h0; i++) {
		for (int j = l1; j <= h1; j++) {
			for (int k = l2; k <= h2; k++) {
				data_next[(i * d1 + j) * d2 + k] = data[(i * d1 + j) * d2 + k] - alpha * ( data[((i - 1) * d1 + j) * d2 + k] +
																						data[((i + 1) * d1 + j) * d2 + k] + data[(i * d1 + (j - 1)) * d2 + k] +
																						data[(i * d1 + (j + 1)) * d2 + k] + data[(i * d1 + j) * d2 + (k - 1)] +
																						data[(i * d1 + j) * d2 + (k + 1)] );
			}
		}
	}
}

int main(int argc, char **argv) {
	int data_depth = 128;
	int data_height = 128;
	int data_width = 128;
	int time_steps = 100;
	bool detailed_print = true;
	// parse command line arguments
	for (int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		if (arg == "-d") {
			data_depth = atoi(argv[++i]);
			data_height = data_depth;
			data_width = data_depth;
		} else if (arg == "--data-depth") {
			data_depth = atoi(argv[++i]);
		} else if (arg == "--data-height") {
			data_height = atoi(argv[++i]);
		} else if (arg == "--data-width") {
			data_width = atoi(argv[++i]);
		} else if (arg == "-t") {
			time_steps = atoi(argv[++i]);
		} else if (arg == "--print-basics") {
			detailed_print = false;
		} else if (arg == "-h" || arg == "--help") {
			cout << "-d" << endl;
			cout << "\tData size dxdxd" << endl;
			cout << "--data-depth" << endl;
			cout << "\tData depth" << endl;
			cout << "--data-height" << endl;
			cout << "\tData height" << endl;
			cout << "--data-width" << endl;
			cout << "\tData width" << endl;
			cout << "-t" << endl;
			cout << "\tNumber of time steps" << endl;
			cout << "--print-basics" << endl;
			cout << "\tPrint basics" << endl;
			cout << "-h or --help" << endl;
			cout << "\tPrint this help information" << endl;
			return 0;
		} else {
			cout << "Unrecognized argument: \"" << arg << "\"" << endl;
			cout << "Use \"" << argv[0] << " -h\" to get help" << endl;
			return -1;
		}
	}
	Dimension data_size(data_depth, data_height, data_width);
	Dimension ghost_cell_dim(1, 1, 1);
	Dimension extended_data_size = data_size + (ghost_cell_dim * 2);
	Dimension low = ghost_cell_dim;
	Dimension high = ((low + data_size) - 1);
	int array_length = extended_data_size(0) * extended_data_size(1) * extended_data_size(2);
	double* data = new double[array_length];
	double* data_next = new double[array_length];
	double t0;
	double elapsed_time;
	initData(data, extended_data_size);
	t0 = getTime();
	for (int i = 0; i < time_steps; i++) {
		compute(data, data_next, low, high, extended_data_size, 0.001);
		swap(data, data_next);
	}
	elapsed_time = getTime() - t0;
	// get average of data
	double sum = 0;
	double element_count = data_size(0) * data_size(1) * data_size(2);
	for (int i = low(0); i <= high(0); i++){
		for (int j = low(1); j <= high(1); j++){
			for (int k = low(2); k <= high(2); k++){
				sum += data[(i * extended_data_size(1) + j) * extended_data_size(2) + k];
			}
		}
	}
	double avg = sum / element_count;
	// calculate gflop
	double total_ops = time_steps * (1E-9 * data_size(0) * data_size(1) * data_size(2)) * 7.0;
	double gflops = total_ops / elapsed_time;
	if (detailed_print) cout << "Implementation: ";
	cout << "Serial" << ",";
	if (detailed_print) cout << "\nData Size: ";
	cout << data_size(0) << "x" << data_size(1) << "x" << data_size(2) << ",";
	if (detailed_print) cout << "\nTime Steps: ";
	cout << time_steps << ",";
	if (detailed_print) cout << "\nElapsed Time (sec): ";
	cout << elapsed_time << ",";
	if (detailed_print) cout << "\nSustained Gflops Rate: " << gflops << ",";
	if (detailed_print) cout <<  "\nAverage of Elements: ";
	cout << avg << endl;
	delete [] data;
	delete [] data_next;
}
