/*
* HeatSimulation.cpp
*/

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <mpi.h>
#include "TiDA.h"

using namespace std;

double getTime() {
	const double kMicro = 1.0e-6;
	struct timeval TV;
	struct timezone TZ;
	const int RC = gettimeofday(&TV, &TZ);
	if(RC == -1) {
		cout << "ERROR: Bad call to gettimeofday\n" << endl;
		return -1;
	}
	return( ((double)TV.tv_sec) + kMicro * ((double)TV.tv_usec));
}

void compute(TileArray& ta, TileArray& ta_next, const float gpu_exec_ratio, const double alpha) {
	ta.updateGhostCells();
	for (TileArray::Iterator i = ta.begin(gpu_exec_ratio), j = ta_next.begin(gpu_exec_ratio); i != ta.end(); ++i, ++j) {
		Tile t = *i;
		Tile t_next = *j;
		TiDA::compute(t_next, t, [](double* data_next, double* data, int depth, int height, int width, int index){
			data_next[index] =	data[index] - 0.001 * (data[index - height * width] + data[index + height * width] + data[index - width] + data[index + width] + data[index - 1] + data[index + 1]);
		});
	}
}

void moveTAtoHost (TileArray& ta) {
	for (TileArray::Iterator i = ta.begin(); i != ta.end(); ++i) {
		Tile t = *i;
	}
}

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	int mpi_size, mpi_rank;
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	int data_depth = 128;
	int data_height = 128;
	int data_width = 128;
	int region_depth = 128;
	int region_height = 128;
	int region_width = 128;
	int time_steps = 100;
	float gpu_exec_ratio = 0;
	bool detailed_print = true;
	// parse command line arguments
	for (int i = 1; i < argc; i++) {
		std::string arg = argv[i];
		if (arg == "-d") {
			data_depth = atoi(argv[++i]);
			data_height = data_depth;
			data_width = data_depth;
			region_depth = data_depth;
			region_height = data_depth;
			region_width = data_depth;
		} else if (arg == "--data-depth") {
			data_depth = atoi(argv[++i]);
			region_depth = data_depth;
		} else if (arg == "--data-height") {
			data_height = atoi(argv[++i]);
			region_height = data_height;
		} else if (arg == "--data-width") {
			data_width = atoi(argv[++i]);
			region_width = data_width;
		} else if (arg == "-r") {
			region_depth = atoi(argv[++i]);
			region_height = region_depth;
			region_width = region_depth;
		} else if (arg == "--region-depth") {
			region_depth = atoi(argv[++i]);
		} else if (arg == "--region-height") {
			region_height = atoi(argv[++i]);
		} else if (arg == "--region-width") {
			region_width = atoi(argv[++i]);
		} else if (arg == "-t") {
			time_steps = atoi(argv[++i]);
		} else if (arg == "-g") {
			gpu_exec_ratio = atof(argv[++i]);
		} else if (arg == "--gpu") {
			gpu_exec_ratio = 1;
		} else if (arg == "--print-basics") {
			detailed_print = false;
		} else if (arg == "-h" || arg == "--help") {
			cout << "-d" << endl;
			cout << "\tData size dxdxd" << endl;
			cout << "--data-depth" << endl;
			cout << "\tData depth" << endl;
			cout << "--data-height" << endl;
			cout << "\tData height" << endl;
			cout << "--data-width" << endl;
			cout << "\tData width" << endl;
			cout << "-r" << endl;
			cout << "\tRegion size rxrxr" << endl;
			cout << "--region-depth" << endl;
			cout << "\tRegion depth" << endl;
			cout << "--region-height" << endl;
			cout << "\tRegion height" << endl;
			cout << "--region-width" << endl;
			cout << "\tRegion width" << endl;
			cout << "-t" << endl;
			cout << "\tNumber of time steps" << endl;
			cout << "-g" << endl;
			cout << "\tRatio of tiles that will be executed on GPU" << endl;
			cout << "--gpu" << endl;
			cout << "\tExecute all on GPU" << endl;
			cout << "--print-basics" << endl;
			cout << "\tPrint basics" << endl;
			cout << "-h or --help" << endl;
			cout << "\tPrint this help information" << endl;
			MPI_Finalize();
			return 0;
		} else {
			cout << "Unrecognized argument: \"" << arg << "\"" << endl;
			cout << "Use \"" << argv[0] << " -h\" to get help" << endl;
			MPI_Finalize();
			return -1;
		}
	}
	if (time_steps % 2 != 0) {
		cout << "Give a time step divisible by 2!";
		return -1;
	}
	int adjusted_time_steps = time_steps / 2;
	Dimension data_size(data_depth, data_height, data_width);
	Dimension region_size(region_depth, region_height, region_width);
	Dimension ghost_size(1, 1, 1);
	Dimension extended_data_size = data_size + (ghost_size * 2);
	TileArray ta(data_size, region_size, ghost_size);
	TileArray ta_next(data_size, region_size, ghost_size);
	// initialize data
	for (TileArray::Iterator i = ta.begin(); i != ta.end(); ++i) {
		Tile t = *i;
		double* data = t.getData();
		Dimension l = t.getLow();	Dimension h = t.getHigh(); Dimension d = t.getPhysDim();
		int init_value = t.getID() * region_depth * region_height * region_width;
		for (int a = l(0); a <= h(0); a++) {
			for (int b = l(1); b <= h(1); b++) {
				for (int c = l(2); c <= h(2); c++) {
					data[(a * d(1) + b) * d(2) + c] = init_value;
					init_value++;
				}
			}
		}
	}
	double t0;
	double elapsed_time;
	t0 = getTime();
	for (int i = 0; i < adjusted_time_steps; i++) {
		compute(ta, ta_next, gpu_exec_ratio, 0.001);
		compute(ta_next, ta, gpu_exec_ratio, 0.001);
	}
	if (gpu_exec_ratio) moveTAtoHost(ta);
	elapsed_time = getTime() - t0;
	// get average of data
	double elements_sum = 0;
	for (TileArray::Iterator i = ta.begin(); i != ta.end(); ++i) {
		Tile t = *i;
		double* data = t.getData();
		Dimension l = t.getLow();	Dimension h = t.getHigh(); Dimension d = t.getPhysDim();
		for (int a = l(0); a <= h(0); a++) {
			for (int b = l(1); b <= h(1); b++) {
				for (int c = l(2); c <= h(2); c++) {
					elements_sum += data[(a * d(1) + b) * d(2) + c];
				}
			}
		}
	}
	double global_sum = 0;
	double global_max_elapsed_time = 0;
	MPI_Reduce(&elements_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&elapsed_time, &global_max_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	if (mpi_rank == 0) {
		double element_count = data_size(0) * data_size(1) * data_size(2);
		double avg = global_sum / element_count;
		double total_ops = time_steps * (1E-9 * data_size(0) * data_size(1) * data_size(2)) * 7.0;
		double gflops = total_ops / global_max_elapsed_time;
		if (detailed_print) cout << "Implementation: ";
		if (gpu_exec_ratio == 0) cout << "TiDA-multigpu - " << mpi_size << " CPU(s),";
		else if (gpu_exec_ratio == 1) cout << "TiDA-multigpu - " << mpi_size << " GPU(s),";
		else {
			int gpu_reg_count = ta.getRegionCount() * gpu_exec_ratio;
			cout << "TiDA-multigpu - " << mpi_size << " CPU(s) and GPU(s) - GPU:CPU Region Ratio = " << gpu_reg_count << ":" << ta.getRegionCount() - gpu_reg_count << ",";
		}
		if (detailed_print) cout << "\nData Size: ";
		cout << data_size(0) << "x" << data_size(1) << "x" << data_size(2) << " - ";
		if (detailed_print) cout << "Region Size: ";
		cout << region_size(0) << "x" << region_size(1) << "x" << region_size(2) << ",";
		if (detailed_print) cout << "\nTime Steps: ";
		cout << time_steps << ",";
		if (detailed_print) cout << "\nElapsed Time (sec): ";
		cout << global_max_elapsed_time << ",";
		if (detailed_print) cout << "\nSustained Gflops Rate: ";
		cout << gflops << ",";
		if (detailed_print) cout << "\nAverage of Elements: ";
		cout << avg << endl;
	}
	MPI_Finalize();
}
