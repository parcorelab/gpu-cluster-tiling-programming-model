/*
* Solves the Panfilov model using an explicit numerical scheme.
* Based on code orginally provided by Xing Cai, Simula Research Laboratory
* and reimplementation by Scott B. Baden, UCSD
*
* Modified and restructured by Didem Unat, Koc University
* Modified and restructured by Burak Bastem, Koc University
*/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <mpi.h>
#include "TiDA.h"

using namespace std;

// utilities
/* Function to plot the 1D array
* 'gnuplot' is instantiated via a pipe and
* the values to be plotted are passed through, along
* with gnuplot commands
* Author : Urvashi R.V. [04/06/2004]
* Modified by Didem Unat [03/23/15]
* Modified by Burak Bastem [01/23/19] */
FILE *gnu=NULL;
void splot(double *U, double simulation_time, int niter, int m, int n) {
  int i, j;
  if(gnu==NULL) gnu = popen("gnuplot","w");
  double mx = -1, mn = 32768;
  for (j=0; j<m; j++){
    for (i=0; i<n; i++){
      if (U[j*n+i] > mx) mx = U[j*n+i];
      if (U[j*n+i] < mn) mn = U[j*n+i];
    }
  }
  fprintf(gnu,"set title \"simulation_time = %f [niter = %d]\"\n",simulation_time, niter);
  fprintf(gnu,"set size square\n");
  fprintf(gnu,"set key off\n");
  fprintf(gnu,"set pm3d map\n");
  // Various color schemes
  fprintf(gnu,"set palette defined (-3 \"blue\", 0 \"white\", 1 \"red\")\n");
  // fprintf(gnu,"set palette rgbformulae 22, 13, 31\n");
  // fprintf(gnu,"set palette rgbformulae 30, 31, 32\n");
  fprintf(gnu,"splot [0:%d] [0:%d][%f:%f] \"-\"\n",m-1,n-1,mn,mx);
  for (j=0; j<m; j++){
    for (i=0; i<n; i++) {
      fprintf(gnu,"%d %d %f\n", i, j, U[i*n+j]);
    }
    fprintf(gnu,"\n");
  }
  fprintf(gnu,"e\n");
  fflush(gnu);
  return;
}

// timer
// make successive calls and take a difference to get the elapsed time.
static const double kMicro = 1.0e-6;
double getTime() {
  struct timeval TV;
  struct timezone TZ;
  const int RC = gettimeofday(&TV, &TZ);
  if (RC == -1) {
    cerr << "ERROR: Bad call to gettimeofday" << endl;
    return(-1);
  }
  return( ((double)TV.tv_sec) + kMicro * ((double)TV.tv_usec) );
}  // end getTime()

void simulate (TileArray* E_ta_ptr, TileArray* E_prev_ta_ptr, TileArray* R_ta_ptr,
  const double alpha, const double kk, const double dt, const double a, const double epsilon,
  const double M1, const double  M2, const double b, const float gpu_exec_ratio) {
    TileArray& E_ta = *E_ta_ptr;
    TileArray& E_prev_ta = *E_prev_ta_ptr;
    TileArray& R_ta = *R_ta_ptr;
    /*
    * Copy data from boundary of the computational box
    * to the padding region, set up for differencing
    * on the boundary of the computational box
    * Using mirror boundaries
    */
    E_prev_ta.updateGhostCells();
    for (TileArray::Iterator i = E_prev_ta.begin(gpu_exec_ratio), j = E_ta.begin(gpu_exec_ratio), k = R_ta.begin(gpu_exec_ratio); i != E_prev_ta.end(); ++i, ++j, ++k) {
      Tile t_e_prev = *i;
      Tile t_e = *j;
      Tile t_r = *k;

      // alternative-1: no temporary variable
      TiDA::compute(t_e_prev, t_e, t_r, [=](double* data_e_prev, double* data_e, double* data_r, const int depth, const int height, const int width, const int index){
        // Solve for the excitation, the PDE
        data_e[index] = data_e_prev[index] + alpha * (data_e_prev[index+1] + data_e_prev[index-1] - 4 * data_e_prev[index] + data_e_prev[index+width] + data_e_prev[index-width]);
        // Solve the ODE, advancing excitation and recovery to the next timtestep
        data_e[index] = data_e[index] - dt * (kk * data_e[index] * (data_e[index] - a) * (data_e[index] - 1) + data_e[index] * data_r[index]);
        data_r[index] = data_r[index] + dt * (epsilon + M1 * data_r[index] / (data_e[index] + M2)) * (-data_r[index] - kk * data_e[index] * (data_e[index] - b - 1));
      });
    }
  }

  void moveTAtoHost (TileArray* ta_ptr) {
    TileArray& ta = *ta_ptr;
    for (TileArray::Iterator i = ta.begin(); i != ta.end(); ++i) {
      Tile t = *i;
    }
  }

  // Main program
  int main (int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int mpi_size, mpi_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int data_height = 128;
    int data_width = 128;
    int region_height = 128;
    int region_width = 128;
    double simulation_time = 1000.0;
    float gpu_exec_ratio = 0;
    int plot_freq = 0;
    bool detailed_print = true;
    // parse command line arguments
    for (int i = 1; i < argc; i++) {
      std::string arg = argv[i];
      if (arg == "-d") {
        data_height = atoi(argv[++i]);
        data_width = data_height;
        region_height = data_height;
        region_width = data_height;
      } else if (arg == "-r") {
        region_height = atoi(argv[++i]);
        region_width = region_height;
      } else if (arg == "--region-height") {
        region_height = atoi(argv[++i]);
      } else if (arg == "--region-width") {
        region_width = atoi(argv[++i]);
      } else if (arg == "-t") {
        simulation_time = atof(argv[++i]);
  		} else if (arg == "-g") {
  			gpu_exec_ratio = atof(argv[++i]);
      } else if (arg == "--gpu") {
        gpu_exec_ratio = 1;
      } else if (arg == "-p") {
        plot_freq = atoi(argv[++i]);
      } else if (arg == "--print-basics") {
        detailed_print = false;
      } else if (arg == "-h" || arg == "--help") {
        cout << "-d" << endl;
        cout << "\tData size dxd" << endl;
        // cout << "--data-height" << endl;
        // cout << "\tData height" << endl;
        // cout << "--data-width" << endl;
        // cout << "\tData width" << endl;
        cout << "-r" << endl;
        cout << "\tRegion size rxr" << endl;
        cout << "--region-height" << endl;
        cout << "\tRegion height" << endl;
        cout << "--region-width" << endl;
        cout << "\tRegion width" << endl;
        cout << "-t" << endl;
        cout << "\tLength of simulation in simulated time units" << endl;
  			cout << "-g" << endl;
  			cout << "\tRatio of tiles that will be executed on GPU" << endl;
  			cout << "--gpu" << endl;
  			cout << "\tExecute all on GPU" << endl;
        cout << "-p" << endl;
        cout << "\tPlot frequency to plot the excitation variable" << endl;
        cout << "--print-basics" << endl;
        cout << "\tPrint basics" << endl;
        cout << "-h or --help" << endl;
        cout << "\tPrint this help information" << endl;
        MPI_Finalize();
        return 0;
      } else {
        cout << "Unrecognized argument: \"" << arg << "\"" << endl;
        cout << "Use \"" << argv[0] << " -h\" to get help" << endl;
        MPI_Finalize();
        return -1;
      }
    }
    /*
    *  Solution arrays
    *   E is the "Excitation" variable, a voltage
    *   R is the "Recovery" variable
    *   E_prev is the Excitation variable for the previous timestep,
    *      and is used in time integration
    */
    // allocate contiguous memory for solution arrays
    // the computational box is defined on [1:m+1,1:n+1]
    // we pad the arrays in order to facilitate differencing on the
    // boundaries of the computation box
    double* E_prev = new double[data_height * data_width];
    double* R = new double[data_height * data_width];
    for (int i = 0; i < data_height; i++) {
      for (int j = 0; j < data_width; j++) {
        E_prev[i * data_width + j] = 0;
      }
    }
    for (int i = 0; i < data_height; i++){
      for (int j = data_width / 2 ; j < data_width; j++){
        E_prev[i * data_width + j] = 1.0;
      }
    }
    for (int i = data_height / 2; i < data_height; i++) {
      for (int j = 0; j < data_width; j++) {
        R[i * data_width + j] = 1.0;
      }
    }
    Dimension data_size(1, data_height, data_width);
    Dimension region_size(1, region_height, region_width);
    Dimension ghost_size(0, 1, 1);
    Dimension extended_data_size = data_size + (ghost_size * 2);
    TileArray E_prev_ta(E_prev, data_size, region_size, ghost_size, false, TiDA::MIRROR_BOUND_UPDATE);
    TileArray E_ta(data_size, region_size, ghost_size, false, TiDA::MIRROR_BOUND_UPDATE);
    TileArray R_ta(R, data_size, region_size, ghost_size, false, TiDA::MIRROR_BOUND_UPDATE);
    TileArray* E_prev_ta_ptr = &E_prev_ta;
    TileArray* E_ta_ptr = &E_ta;
    TileArray* R_ta_ptr = &R_ta;
    free(E_prev);
    free(R);
    // various constants - these definitions shouldn't change
    const double a = 0.1, b = 0.1, kk = 8.0, M1 = 0.07, M2 = 0.3, epsilon = 0.01, d = 5e-5;
    double dx = 1.0 / data_width;
    // for time integration, these values shouldn't change
    double rp = kk * (b+1) * (b+1) / 4;
    double dte = (dx * dx) / (d * 4 + ((dx * dx)) * (rp + kk));
    double dtr = 1 / (epsilon + ((M1 / M2) * rp));
    double dt = (dte < dtr) ? 0.95 * dte : 0.95 * dtr;
    double alpha = d * dt / (dx * dx);
    // start the timer
    double t0 = getTime();
    // simulated time is different from the integer timestep number
    // simulated time
    double t = 0.0;
    // integer timestep number
    int niter = 0;
    while (t < simulation_time) {
      t += dt;
      niter++;
      simulate(E_ta_ptr, E_prev_ta_ptr, R_ta_ptr, alpha, kk, dt, a, epsilon, M1, M2, b, gpu_exec_ratio);
      //swap current E with previous E
      TileArray* tmp = E_ta_ptr; E_ta_ptr = E_prev_ta_ptr; E_prev_ta_ptr = tmp;
      if (plot_freq){
        int k = (int) (t / plot_freq);
        if ((t - k * plot_freq) < dt){
          // plotting is unavailable for the moment
          // splot(E,t,niter,m+2,n+2); //n and m are data height and width
        }
      }
    } //end of while loop
    if (gpu_exec_ratio) moveTAtoHost(E_prev_ta_ptr);
    double elapsed_time = getTime() - t0;
    // get statistics
    TileArray& ta = *E_prev_ta_ptr;
    double max = -1;
    double l2norm = 0;
    for (TileArray::Iterator i = ta.begin(); i != ta.end(); ++i) {
      Tile t = *i;
      TiDA::compute_with_capture(t, [&](double* data, int depth, int height, int width, int index){
        l2norm += data[index] * data[index];
        if(data[index] > max) max = data[index];
      });
    }
    double global_max = -1;
    double global_l2norm = 0;
    double global_max_elapsed_time = 0;
    MPI_Reduce(&l2norm, &global_l2norm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&max, &global_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&elapsed_time, &global_max_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    if (mpi_rank == 0) {
      global_l2norm /= (double) (data_height * data_width);
      global_l2norm = sqrt(global_l2norm);
      double gflops = (double) (niter * (1E-9 * data_height * data_width) * 28.0) / global_max_elapsed_time;
      double bandwidth = (double) (niter * 1E-9 * (data_height * data_width * sizeof(double) * 4.0)) / global_max_elapsed_time;
      if (detailed_print) cout << "Implementation: ";
      if (gpu_exec_ratio == 0) cout << "TiDA-multigpu - " << mpi_size << " CPU(s),";
  		else if (gpu_exec_ratio == 1) cout << "TiDA-multigpu - " << mpi_size << " GPU(s),";
  		else {
  			int gpu_reg_count = ta.getRegionCount() * gpu_exec_ratio;
  			cout << "TiDA-multigpu - " << mpi_size << " CPU(s) and GPU(s) - GPU:CPU Region Ratio = " << gpu_reg_count << ":" << ta.getRegionCount() - gpu_reg_count << ",";
  		}
      if (detailed_print) cout << "\nData Size: ";
      cout << data_height << "x" << data_width << " - ";
      if (detailed_print) cout << "Region Size: ";
      cout << region_height << "x" << region_width << ",";
      if (detailed_print) cout << "\nDuration of Simulation: ";
      cout << simulation_time << ",";
      if (detailed_print) cout << "\nTime Step (dt): " << dt << ",";
      if (detailed_print) cout << "\nNumber of iterations: " << niter << ",";
      if (detailed_print) cout << "\nElapsed Time (sec): ";
      cout << global_max_elapsed_time << ",";
      if (detailed_print) cout << "\nSustained Gflops Rate: ";
      cout << gflops << ",";
      if (detailed_print) cout << "\nSustained Bandwidth (GB/sec): ";
      cout << bandwidth << ",";
      if (detailed_print) cout << "\nMax: ";
      cout << global_max << " - ";
      if (detailed_print) cout << "L2norm: ";
      cout << global_l2norm << endl;
    }
    if (plot_freq){
      cout << "\nEnter any input to close the program and the plot..." << endl;
      getchar();
    }
    MPI_Finalize();
    return 0;
  }
