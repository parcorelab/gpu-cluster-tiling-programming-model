/*
* Solves the Panfilov model using an explicit numerical scheme.
* Based on code orginally provided by Xing Cai, Simula Research Laboratory
* and reimplementation by Scott B. Baden, UCSD
*
* Modified and  restructured by Didem Unat, Koc University
* Modified and restructured by Burak Bastem, Koc University
*/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <math.h>
#include <sys/time.h>

using namespace std;

// Utilities
/* Function to plot the 1D array
* 'gnuplot' is instantiated via a pipe and
* the values to be plotted are passed through, along
* with gnuplot commands
* Author : Urvashi R.V. [04/06/2004]
* Modified by Didem Unat [03/23/15]
* Modified by Burak Bastem [01/23/19] */
FILE *gnu = NULL;
void splot(double *U, double simulation_time, int niter, int ext_data_height, int ext_data_width) {
  if(gnu == NULL) gnu = popen("gnuplot","w");
  double max = -1, min = 32768;
  for (int i = 0; i < ext_data_height; i++){
    for (int j = 0; j < ext_data_width; j++){
      if (U[i * ext_data_width + j] > max) max = U[i * ext_data_width + j];
      if (U[i * ext_data_width + j] < min) min = U[i * ext_data_width + j];
    }
  }
  fprintf(gnu, "set title \"T = %f [niter = %d]\"\n", simulation_time, niter);
  fprintf(gnu, "set size square\n");
  fprintf(gnu, "set key off\n");
  fprintf(gnu, "set pm3d map\n");
  // Various color schemes
  fprintf(gnu, "set palette defined (-3 \"blue\", 0 \"white\", 1 \"red\")\n");
  // fprintf(gnu,"set palette rgbformulae 22, 13, 31\n");
  // fprintf(gnu,"set palette rgbformulae 30, 31, 32\n");
  fprintf(gnu, "splot [0:%d] [0:%d][%f:%f] \"-\"\n", ext_data_height - 1, ext_data_width - 1, min, max);
  for (int i = 0; i < ext_data_height; i++){
    for (int j = 0; j < ext_data_width; j++) {
      fprintf(gnu, "%d %d %f\n", j, i, U[j * ext_data_width + i]);
    }
    fprintf(gnu, "\n");
  }
  fprintf(gnu, "e\n");
  fflush(gnu);
  return;
}

// Timer
// Make successive calls and take a difference to get the elapsed time.
static const double kMicro = 1.0e-6;
double getTime() {
  struct timeval TV;
  struct timezone TZ;
  const int RC = gettimeofday(&TV, &TZ);
  if(RC == -1) {
    cerr << "ERROR: Bad call to gettimeofday" << endl;
    return(-1);
  }
  return( ((double)TV.tv_sec) + kMicro * ((double)TV.tv_usec) );
}  // end getTime()

// Reports statistics about the computation
// These values should not vary (except to within roundoff)
// when we use different numbers of  processes to solve the problem
double stats(double *E, int data_height, int data_width, double *_mx) {
  double max = -1;
  double l2norm = 0;
  for (int i = 1; i <= data_height; i++){
    for (int j = 1; j <= data_width; j++){
      l2norm += E[i * (data_width + 2) + j] * E[i * (data_width + 2) + j];
      if (E[i * (data_width + 2) + j] > max) max = E[i * (data_width + 2) + j];
    }
  }
  *_mx = max;
  l2norm /= (double) (data_height * data_width);
  l2norm = sqrt(l2norm);
  return l2norm;
}

void simulate (double* E,  double* E_prev, double* R,
  const double alpha, const int data_height, const int data_width,
  const double kk, const double dt, const double a, const double epsilon,
  const double M1,const double  M2, const double b) {
    int ext_data_height = data_height + 2;
    int ext_data_width = data_width + 2;
    int i, j; // this is the way in original code and is faster!
    /*
    * Copy data from boundary of the computational box
    * to the padding region, set up for differencing
    * on the boundary of the computational box
    * Using mirror boundaries
    */
    for (i = 1; i <= data_height; i++){
      E_prev[i * ext_data_width] = E_prev[i * ext_data_width + 2];
    }
    for (i = 1; i <= data_height; i++){
      E_prev[(i + 1) * ext_data_width - 1] = E_prev[(i + 1) * ext_data_width - 3];
    }
    for (j = 1; j <= data_width; j++){
      E_prev[j] = E_prev[2 * ext_data_width + j];
    }
    for (j = 1; j <= data_width; j++){
      E_prev[(ext_data_height - 1) * ext_data_width + j] = E_prev[(ext_data_height - 3) * ext_data_width + j];
    }
    // Solve for the excitation, the PDE
    for (i = 1; i <= data_height; i++){
      for (j = 1; j <= data_width; j++) {
        E[i * ext_data_width + j] = E_prev[i * ext_data_width + j] + alpha * (E_prev[i * ext_data_width + (j + 1)] + E_prev[i * ext_data_width + (j - 1)] - 4 * E_prev[i * ext_data_width + j] + E_prev[(i + 1) * ext_data_width + j] + E_prev[(i - 1) * ext_data_width + j]);
      }
    }
    /*
    * Solve the ODE, advancing excitation and recovery to the
    *     next timtestep
    */
    for (i = 1; i <= data_height; i++){
      for (j = 1; j <= data_width; j++) {
        E[i * ext_data_width + j] = E[i * ext_data_width + j] - dt * (kk* E[i * ext_data_width + j] * (E[i * ext_data_width + j] - a) * (E[i * ext_data_width + j] - 1) + E[i * ext_data_width + j] * R[i * ext_data_width + j]);
      }
    }
    for (i = 1; i <= data_height; i++){
      for (j = 1; j <= data_width; j++) {
        R[i * ext_data_width + j] = R[i * ext_data_width + j] + dt * (epsilon + M1 * R[i * ext_data_width + j] / ( E[i * ext_data_width + j] + M2)) * (-R[i * ext_data_width + j] - kk * E[i * ext_data_width + j] * (E[i * ext_data_width + j] - b - 1));
      }
    }
  }

  // Main program
  int main (int argc, char** argv) {
    int data_height = 128;
    int data_width = 128;
    double simulation_time = 1000.0;
    int plot_freq = 0;
    bool detailed_print = true;
    // parse command line arguments
    for (int i = 1; i < argc; i++) {
      std::string arg = argv[i];
      if (arg == "-d") {
        data_height = atoi(argv[++i]);
        data_width = data_height;
      } else if (arg == "-t") {
        simulation_time = atof(argv[++i]);
      } else if (arg == "-p") {
        plot_freq = atoi(argv[++i]);
      } else if (arg == "--print-basics") {
        detailed_print = false;
      } else if (arg == "-h" || arg == "--help") {
        cout << "-d" << endl;
        cout << "\tData size dxd" << endl;
        // cout << "--data-height" << endl;
        // cout << "\tData height" << endl;
        // cout << "--data-width" << endl;
        // cout << "\tData width" << endl;
        cout << "-t" << endl;
        cout << "\tLength of simulation in simulated time units" << endl;
        cout << "-p" << endl;
        cout << "\tPlot frequency to plot the excitation variable" << endl;
        cout << "--print-basics" << endl;
        cout << "\tPrint basics" << endl;
        cout << "-h or --help" << endl;
        cout << "\tPrint this help information" << endl;
        return 0;
      } else {
        cout << "Unrecognized argument: \"" << arg << "\"" << endl;
        cout << "Use \"" << argv[0] << " -h\" to get help" << endl;
        return -1;
      }
    }
    /*
    *  Solution arrays
    *   E is the "Excitation" variable, a voltage
    *   R is the "Recovery" variable
    *   E_prev is the Excitation variable for the previous timestep,
    *      and is used in time integration
    */
    double *E, *R, *E_prev;
    // Various constants - these definitions shouldn't change
    const double a = 0.1, b = 0.1, kk = 8.0, M1 = 0.07, M2 = 0.3, epsilon = 0.01, d = 5e-5;
    // Allocate contiguous memory for solution arrays
    // The computational box is defined on [1:m+1,1:n+1]
    // We pad the arrays in order to facilitate differencing on the
    // boundaries of the computation box
    int ext_data_height = data_height + 2;
    int ext_data_width = data_width + 2;
    size_t size_of_arrays = ext_data_height * ext_data_width * sizeof(double);
    E = (double *)malloc(size_of_arrays);
    R = (double *)malloc(size_of_arrays);
    E_prev = (double *)malloc(size_of_arrays);
    // Initialization
    for (int i = 1; i <= data_height; i++) {
      for (int j = 1; j <= data_width; j++) {
        E_prev[i * ext_data_width + j] = R[i * ext_data_width + j] = 0;
      }
    }
    for (int i = 1; i <= data_height; i++){
      for (int j = data_width / 2 + 1; j <= data_width; j++){
        E_prev[i * ext_data_width + j] = 1.0;
      }
    }
    for (int i = data_height / 2 + 1; i <= data_height; i++) {
      for (int j = 1; j <= data_width; j++) {
        R[i * ext_data_width + j] = 1.0;
      }
    }
    double dx = 1.0 / data_width;
    // For time integration, these values shouldn't change
    double rp = kk * (b + 1) * (b + 1) / 4;
    double dte = (dx * dx) / (d * 4 + ((dx * dx)) * (rp + kk));
    double dtr = 1 / (epsilon + ((M1 / M2) * rp));
    double dt = (dte < dtr) ? 0.95 * dte : 0.95 * dtr;
    double alpha = d * dt / (dx * dx);
    // Start the timer
    double t0 = getTime();
    // Simulated time is different from the integer timestep number
    // Simulated time
    double t = 0.0;
    // Integer timestep number
    int niter=0;
    while (t<simulation_time){
      t += dt;
      niter++;
      simulate(E, E_prev, R, alpha, data_width, data_height, kk, dt, a, epsilon, M1, M2, b);
      //swap current E with previous E
      double *tmp = E; E = E_prev; E_prev = tmp;
      if (plot_freq){
        int k = (int)(t / plot_freq);
        if ((t - k * plot_freq) < dt){
          splot(E, t, niter, ext_data_height, ext_data_width);
        }
      }
    }//end of while loop
    double elapsed_time = getTime() - t0;
    double gflops = (double) (niter * (1E-9 * data_height * data_width) * 28.0) / elapsed_time;
    double bandwidth = (double) (niter * 1E-9 * (data_height * data_width * sizeof(double) * 4.0)) / elapsed_time;
    double max;
    double l2norm = stats(E_prev,data_height,data_width,&max);
    if (detailed_print) cout << "Implementation: ";
    else cout << "Serial 1D,";
    if (detailed_print) cout << "\nData Size: ";
    cout << data_height << "x" << data_width << ",";
    if (detailed_print) cout << "\nDuration of Simulation: ";
    cout << simulation_time << ",";
    if (detailed_print) cout << "\nTime Step (dt): " << dt << ",";
    if (detailed_print) cout << "\nNumber of iterations: " << niter << ",";
    if (detailed_print) cout << "\nElapsed Time (sec): ";
    cout << elapsed_time << ",";
    if (detailed_print) cout << "\nSustained Gflops Rate: ";
    cout << gflops << ",";
    if (detailed_print) cout << "\nSustained Bandwidth (GB/sec): ";
    cout << bandwidth << ",";
    if (detailed_print) cout << "\nMax: ";
    cout << max << " - ";
    if (detailed_print) cout << "L2norm: ";
    cout << l2norm << endl;
    if (plot_freq){
      cout << "\n\nEnter any input to close the program and the plot..." << endl;
      getchar();
    }
    free(E);
    free(E_prev);
    free(R);
    return 0;
  }
